<?php use Xdire\Dude\Core\App; use Xdire\Dude\Core\Server\Request; use Xdire\Dude\Core\Server\Response;


/** -------------------------------------- DEFINE MIDDLEWARE CONTROLLERS ------------------------------------------ */
$authByAuthAPI = new \App\Middleware\AuthApiMiddleware();
$optMiddleware = new \App\Middleware\OptMiddleware();


/** ------------------------------------------ DEFINE ROUTING BELOW ----------------------------------------------- */
App::route(ROUTE_ALL,"/",function($req, Response $res) {
    $res->send(200,"Api said hello to you!");
});

/*
 *  ADD ITEMS OR VARIATIONS
 */
App::route(ROUTE_POST,"/api/v1/item/add",function(Request $req,Response $res) {
    App::routeNextController(new \App\Controller\Items\AddNewItemController(), $req, $res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_POST,"/api/v1/itemvar/add",function(Request $req,Response $res) {
    App::routeNextController(new \App\Controller\ItemVariations\AddNewItemVariationController(), $req, $res);
},$authByAuthAPI, $optMiddleware);


/*
 *  GET ITEMS
 */
App::route(ROUTE_GET,"/api/v1/item/id/*id",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\Items\GetItemByIdController(),$req,$res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/item/varid/*id",function(Request $req, Response $res) {
    
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/item/varcode/*code",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\Items\GetItemByVariationCode(), $req, $res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/item/style/*style",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\Items\GetByVariationStyle(), $req, $res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/item/varparams",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\Items\GetItemByVariationParams(), $req, $res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/item/short/*varid",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\Items\GetSingleItemByVariationId(), $req, $res);
},$authByAuthAPI, $optMiddleware);

/*
 *  GET VARIATIONS
 */
App::route(ROUTE_GET,"/api/v1/variation/id/*id",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\ItemVariations\GetItemVariationById(), $req, $res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/variation/code/*code",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\ItemVariations\GetItemVariationByCode(), $req, $res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/variation/itemid/*id",function(Request $req, Response $res) {

},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/variation/customid/*idset",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\ItemVariations\GetVariationByCustomId(), $req, $res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/variation/min/customid/*idset",function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\ItemVariations\GetVariationByCustomIdMin(), $req, $res);
},$authByAuthAPI, $optMiddleware);

/*
 *  UPDATE VARIATIONS
 */
App::route(ROUTE_PUT,"/api/v1/variation", function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\ItemVariations\UpdateItemVariation(), $req, $res);
},$authByAuthAPI, $optMiddleware);

/*
 *  GET STYLES
 */
App::route(ROUTE_GET,"/api/v1/style/list/*code", function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\Styles\GetStyleList(), $req, $res);
},$authByAuthAPI, $optMiddleware);

App::route(ROUTE_GET,"/api/v1/style/item/*id", function(Request $req, Response $res) {
    App::routeNextController(new \App\Controller\Styles\GetStyleByItemId(), $req, $res);
},$authByAuthAPI, $optMiddleware);

/*
 *  SERVICE ROUTES
 */

App::route(ROUTE_GET,"/api/v1/srv/recache/image", function(Request $req, Response $res){
   App::routeNextController(new \App\Controller\System\Recache\ImageRecache(),$req,$res);
});