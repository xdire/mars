<?php namespace App\Model\SizeModel;

class SizeEntity
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $real = "";

    /**
     * SizeEntity constructor.
     * @param string $name
     */
    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool {
        return isset($this->id) ? false : true;
    }

    /**
     * @return int | null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string | null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string | null
     */
    public function getRealName() : string
    {
        return $this->real;
    }

    /**
     * @param string $real
     */
    public function setRealName($real)
    {
        $this->real = $real;
    }


}