<?php namespace App\Model\SizeModel;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use Xdire\Dude\Core\DB\DBO;

/**
 * Class SizeDAO
 * @package App\Model\SizeModel
 */
class SizeDAO extends DBO
{

    /**
     * @param SizeEntity $e
     * @return SizeEntity
     * @throws \Xdire\Dude\Core\DB\DBWriteException
     */
    public function save(SizeEntity $e) : SizeEntity {

        $e->setName(StringsHelper::escapeInput($e->getName()));

        if($e->getRealName() == "")
            $e->setRealName($e->getName());

        $query = "INSERT INTO Sizes ".
            "(sysven, size_name, size_real) ".
            "VALUES (".WorkingSet::getUserEntity()->getVendorId().",'".$e->getName()."','".$e->getRealName()."');";

        $this->writeBegin($query);

        if($this->getLastInsertId() > 0)
            $e->setId($this->getLastInsertId());

        return $e;

    }

    /**
     * @param string $name
     * @return SizeEntity
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByName(string $name) : SizeEntity {

        $e = new SizeEntity($name);

        $query = "SELECT * FROM Sizes ".
            "WHERE sysven = ".WorkingSet::getUserEntity()->getVendorId()." AND size_name = '".StringsHelper::escapeInput($name)."';";

        $r = $this->selectBegin($query);

        if($row = $this->fetchRow($r)){

            $e->setId($row['size_id']);
            $e->setName($row['size_name']);
            $e->setRealName($row['size_real']);

        }

        return $e;

    }

    /**
     * @param int $id
     * @return SizeEntity
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getById(int $id) : SizeEntity {

        $query = "SELECT * FROM Sizes ".
            "WHERE size_id = ".$id." AND sysven = ".WorkingSet::getUserEntity()->getVendorId().";";

        $r = $this->selectBegin($query);

        $e = new SizeEntity("");

        if($row = $this->fetchRow($r)){

            $e->setId($row['size_id']);
            $e->setName($row['size_name']);
            $e->setRealName($row['size_real']);

        }

        return $e;

    }

}