<?php namespace App\Model\ItemModel;

use App\Model\BrandModel\BrandEntity;
use App\Model\ItemCategoryModel\ItemCategoryEntity;
use App\Model\ItemCategoryModel\ItemCategoryNode;
use App\Model\ItemCommon\ItemDescriptionEntity;
use App\Model\ItemCommon\ItemInformationEntity;
use App\Model\ItemVariationModel\ItemVariationEntity;
use App\Model\StyleModel\StyleEntity;

class ItemEntity
{
    /** @var int  */
    private $id;

    /** @var string */
    private $customId;

    /** @var int */
    private $ownerId;

    /** @var BrandEntity */
    private $brandObject;

    /** @var ItemCategoryNode */
    private $categoryEntity;
    
    /** @var ItemCategoryEntity */
    private $categoryObject;

    /** @var string */
    private $title = "";

    /** @var ItemInformationEntity */
    private $infoObject;

    /** @var ItemDescriptionEntity */
    private $descObject;

    /** @var StyleEntity */
    private $style;

    /** @var int */
    private $variationsAmount = 0;

    /** @var ItemVariationEntity[] */
    private $variations = [];

    function __construct()
    {
        $this->style = new StyleEntity();
        $this->brandObject = new BrandEntity();
        $this->categoryEntity = new ItemCategoryNode();
        $this->categoryObject = new ItemCategoryEntity();
        $this->infoObject = new ItemInformationEntity();
        $this->descObject = new ItemDescriptionEntity();
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool {
        return isset($this->id) ? false : true;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @param int $ownerId
     */
    public function setOwnerId(int $ownerId)
    {
        $this->ownerId = $ownerId;
    }

    /**
     * @return BrandEntity | null
     */
    public function getBrand() : BrandEntity
    {
        return $this->brandObject;
    }

    /**
     * @param BrandEntity $brandObject
     */
    public function setBrandObject(BrandEntity $brandObject)
    {
        $this->brandObject = $brandObject;
    }


    /**
     * @return ItemCategoryNode
     */
    public function getCategory() : ItemCategoryNode
    {
        return $this->categoryEntity;
    }

    /**
     * @param ItemCategoryNode $categoryEntity
     */
    public function setCategory(ItemCategoryNode $categoryEntity)
    {
        $this->categoryEntity = $categoryEntity;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return ItemDescriptionEntity
     */
    public function &getDescObject() : ItemDescriptionEntity
    {
        return $this->descObject;
    }

    /**
     * @param ItemDescriptionEntity $descObject
     */
    public function setDescObject(ItemDescriptionEntity $descObject)
    {
        $this->descObject = $descObject;
    }

    /**
     * @return int
     */
    public function getVariationsAmount() : int
    {
        return $this->variationsAmount;
    }

    /**
     * @return ItemVariationEntity[]
     */
    public function &getVariations() : array
    {
        return $this->variations;
    }

    /**
     * @param ItemVariationEntity $variation
     */
    public function setVariation(ItemVariationEntity $variation)
    {
        $variation->setBrand($this->brandObject->getName());
        $variation->setTitle($this->title);

        $this->variations[$this->variationsAmount++] = $variation;
    }

    /**
     * @return ItemCategoryEntity
     */
    public function &getCategoryObject() : ItemCategoryEntity
    {
        return $this->categoryObject;
    }

    /**
     * @param ItemCategoryEntity $categoryObject
     */
    public function setCategoryObject(ItemCategoryEntity $categoryObject)
    {
        $this->categoryObject = $categoryObject;
    }

    /**
     * @return ItemInformationEntity
     */
    public function &getInformationObject() : ItemInformationEntity
    {
        return $this->infoObject;
    }

    /**
     * @param ItemInformationEntity $infoObject
     */
    public function setInformationObject(ItemInformationEntity $infoObject)
    {
        $this->infoObject = $infoObject;
    }

    /**
     * @return StyleEntity
     */
    public function &getStyle()
    {
        return $this->style;
    }

    /**
     * @param StyleEntity $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @return string
     */
    public function getCustomId()
    {
        return $this->customId;
    }

    /**
     * @param string $customId
     */
    public function setCustomId($customId)
    {
        $this->customId = (string)$customId;
    }

    /**
     * @return array
     */
    public function toArray() : array {

        $cats = isset($this->categoryObject) ? $this->categoryObject->toArray() : [];

        $a = [
            "id" => $this->id,
            "customId" => $this->customId,
            "title" => $this->title,
            "brandId" => $this->brandObject->getId(),
            "brand" => $this->brandObject->getName(),
            "category" => $cats,
            "style" => $this->style->toArray(),
            "info" => $this->infoObject->toArray(),
            "description" => $this->descObject->toArray(),
            "variations" => $this->_variationsToArray()
        ];

        return $a;
    }

    public function toMinArray(): array {
        return [];
    }

    /**
     * @return string
     * @throws \RuntimeException
     */
    public function toJson() : string {
        if($str = json_encode($this->toArray(),JSON_UNESCAPED_SLASHES))
            return $str;

        throw new \RuntimeException(500,"Item is corrupted");
    }

    /**
     * @return array
     */
    private function _variationsToArray() : array {
        $a = [];
        foreach ($this->variations as $variation){
            $a[] = $variation->toArray();
        }
        return $a;
    }

}