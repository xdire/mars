<?php namespace App\Model\ItemModel;

use App\Model\ItemAbstract\ItemAbstractStorageArranger;

/**
 * Class ItemSaveNewEntity
 *
 * @package App\Model\ItemModel
 */
class ItemSaveNewEntity extends ItemAbstractStorageArranger
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param ItemEntity $e
     * @return ItemEntity
     */
    public function save(ItemEntity &$e) : ItemEntity {

        // Start with dissolving the brand entity to get a proper ID for saving the ItemEntity
        $this->_checkBrand($e);

        // Check categories object
        $this->_checkCategory($e);

        // Check images which attached to the item object
        $this->_checkImages($e);

        // Continue with other data
        $this->_checkVariants($e);

        $idao = new ItemDAO();

        $idao->save($e);

        //vd($e);
        return $e;

    }

    /**
     * @param ItemEntity $e
     */
    private function _checkVariants(ItemEntity &$e) {

        if($vars = $e->getVariations()) {

            foreach ($vars as &$var) {

                // Check IV Color Object
                $this->_checkVariatonColor($var);

                // Check IV Size Object
                $this->_checkVariationSize($var);

                // Check IV Attributes
                $this->_checkVariationAttributes($var);

                // Check IV Variation Info object
                $this->_checkVariationInfo($var);

            }

        }

    }

}