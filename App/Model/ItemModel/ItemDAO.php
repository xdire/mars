<?php namespace App\Model\ItemModel;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use App\Model\ItemVariationModel\ItemVariationEntity;
use Xdire\Dude\Core\DB\DBException;
use Xdire\Dude\Core\DB\DBO;

class ItemDAO extends DBO
{

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  Save ItemEntity to Database
     *
     *  will also save all found linkage parameters for an Item
     *
     *  @param ItemEntity $e
     *  @return ItemEntity
     */
    public function save(ItemEntity &$e) : ItemEntity {

        $uid = WorkingSet::getUserEntity()->getVendorId();

        $cat = (!empty($e->getCategory()->getId())) ? $e->getCategory()->getId() : 0;

        try {

            $this->transactionStart();

            // ------------------------------------------
            //              Save item
            // ------------------------------------------

            $query = "INSERT INTO Items ".
                "(sysvid, brand, category, title, info, description, catalog, styles, custom_id) ".
                "VALUES ".
                "(".$uid.",".$e->getBrand()->getId().",".$cat.",'".StringsHelper::escapeInput($e->getTitle())."','".
                StringsHelper::escapeJson($e->getInformationObject()->toJson())."','".
                StringsHelper::escapeJson($e->getDescObject()->toJson())."','".
                StringsHelper::escapeJson($e->getCategoryObject()->toJson())."','".
                StringsHelper::escapeJson($e->getStyle()->toJson())."','".
                StringsHelper::escapeJson($e->getCustomId())."')";

            $this->writeBegin($query);

            if($this->getLastInsertId() > 0) {

                $e->setId($this->getLastInsertId());

                // Save title image link to Item
                if($imgt = $e->getInformationObject()->getItemTitleImage()) {

                    if($imgt->getId()) {

                        $query = "INSERT INTO ItemImages " .
                            "(item_id, item_vid, image_id) " .
                            "VALUES " .
                            "(" . $e->getId() . ",0," . $imgt->getId() . ")";

                        $this->writeBegin($query);

                    }

                }

                // Save links to ImageSet
                $imgs = $e->getInformationObject()->getItemImagesSet();

                foreach ($imgs as $imageData) {

                    if($imageData->getId()) {

                        $query = "INSERT INTO ItemImages " .
                            "(item_id, item_vid, image_id) " .
                            "VALUES " .
                            "(" . $e->getId() . ",0," . $imageData->getId() . ")";

                        $this->writeBegin($query);

                    }

                }

                $styl = $e->getStyle();

                foreach ($styl->getStyles() as $style) {

                    $query = "INSERT INTO ItemStyles (sysvid, style_code, vendor_name, item_id) VALUES ".
                        "(".$uid.",'".$style->getName()."','".$style->getVendor()."','".$e->getId()."')";
                    $this->writeBegin($query);

                }
                // Save attributes linkage

                // .....
                // .......

                // ------------------------------------------
                //          Save variation then
                // ------------------------------------------

                foreach ($e->getVariations() as &$variation) {

                    $sizeId = !empty($variation->getSize()->getId()) ? $variation->getSize()->getId() : null;
                    $colrId = !empty($variation->getColor()->getId()) ? $variation->getColor()->getId() : null;

                    $query = "INSERT INTO ItemVariations ".
                        "(sysvid, parent_id, size_id, color_id, code, weight, height, width, price, custom_vid) VALUES ".
                        "(".$uid.",".$e->getId().",".$sizeId.",".$colrId.",'".StringsHelper::escapeInput($variation->getCode())."',".
                        $variation->getWeight().",".$variation->getHeight().",".$variation->getWidth().",".
                        $variation->getPrice().",'".
                        StringsHelper::escapeJson($variation->getCustomId())."')";;

                    $this->writeBegin($query);

                    if($this->getLastInsertId() > 0) {

                        $variation->setId((int)$this->getLastInsertId());

                        // Save title image link to Variation
                        if($imgt = $variation->getInformationObject()->getItemTitleImage()) {

                            if($imgt->getId()) {

                                $query = "INSERT INTO ItemImages " .
                                    "(item_id, item_vid, image_id) " .
                                    "VALUES " .
                                    "(" . $e->getId() . "," . $variation->getId() . "," . $imgt->getId() . ")";

                                $this->writeBegin($query);

                            }

                        }

                        // Save links to ImageSet for Variation
                        $imgs = $variation->getInformationObject()->getItemImagesSet();

                        foreach ($imgs as $imageData) {

                            if($imageData->getId()) {

                                $query = "INSERT INTO ItemImages " .
                                    "(item_id, item_vid, image_id) " .
                                    "VALUES " .
                                    "(" . $e->getId() . "," . $variation->getId() . "," . $imageData->getId() . ")";

                                $this->writeBegin($query);
                                
                            }

                        }

                        // Save links to Attributes
                        $attr = $variation->getAttributes();

                        foreach ($attr->getAttributes() as $attr) {
                            
                            $query = "INSERT INTO ItemAttributes (attr_id, item_vid) VALUES (".$attr->getId().",".$variation->getId().")";
                            $this->writeBegin($query);

                        }

                        // -----------------------------------------------------------------
                        //          Save cached information for variation at the end
                        // -----------------------------------------------------------------

                        $query = "INSERT INTO VariationCached ".
                            "(varId, title, brand_name, color_name, size_name, attributes, vardata) VALUES ".
                            "(".$variation->getId().",'".
                            StringsHelper::escapeInput($variation->getTitle())."','".
                            StringsHelper::escapeInput($variation->getBrand())."','".
                            StringsHelper::escapeInput($variation->getColor()->getName())."','".
                            StringsHelper::escapeInput($variation->getSize()->getName())."','".
                            StringsHelper::escapeJson($variation->getAttributes()->toJson())."','".
                            StringsHelper::escapeJson($variation->getInformationObject()->toJson())."')";

                        $this->writeBegin($query);

                    }

                }

            }

            $this->transactionCommit();
            return $e;

        } catch (DBException $e) {

            vd($e);
            $this->transactionCancel();
            throw new \RuntimeException($e->getMessage(), $e->getCode());

        }

    }

    public function update(ItemEntity $e) : ItemEntity {

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  Get ItemEntity by ItemId
     *
     *  @param int $id
     *  @return ItemEntity
     *  @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getById(int $id) : ItemEntity {

        $e = new ItemEntity();
        
        $query = "SELECT * FROM Items ".
            "LEFT JOIN ItemVariations ON Items.id = ItemVariations.parent_id ".
            "LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE Items.id = ".(int)$id;

        $r = $this->selectBegin($query);

        while ($row = $this->fetchRow($r)){

            $this->_assembleEntity($e,$row);

        }

        return $e;

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param string $code
     *  @return ItemEntity[]
     *  @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByVariationCode(string $code) : array {

        $e = new ItemEntity();

        $query = "SELECT * FROM ItemVariations ".
            "LEFT JOIN Items ON Items.id = ItemVariations.parent_id ".
            "LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE ItemVariations.sysvid = ".WorkingSet::getUserEntity()->getVendorId()." AND ItemVariations.code LIKE '%".StringsHelper::escapeInput($code)."'";

        $r = $this->selectBegin($query);

        $a = [];
        $i = 0;

        $itemid = null;

        while ($row = $this->fetchRow($r)) {

            if($itemid != $row['id']) {
                $ent = new ItemEntity();
                $a[$i] = $ent;
                $e = &$a[$i++];
            }

            $this->_assembleEntity($e,$row);
            $itemid = $row['id'];
        }

        return $a;

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param string $style
     *  @return ItemEntity[]
     *  @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByVariationStyle(string $style) : array {

        $e = new ItemEntity();

        $query = "SELECT * FROM ItemStyles ".
            "LEFT JOIN Items ON ItemStyles.item_id = Items.id ".
            "LEFT JOIN ItemVariations ON Items.id = ItemVariations.parent_id ".
            "LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE ItemStyles.sysvid = ".WorkingSet::getUserEntity()->getVendorId()." ".
            "AND ItemStyles.style_code = '".StringsHelper::escapeInput($style)."' GROUP BY ItemVariations.var_id";

        $r = $this->selectBegin($query);

        $a = [];
        $i = 0;

        $itemid = null;

        while ($row = $this->fetchRow($r)) {

            if($itemid != $row['id']) {
                $ent = new ItemEntity();
                $a[$i] = $ent;
                $e = &$a[$i++];
            }

            $this->_assembleEntity($e,$row);
            $itemid = $row['id'];

        }

        return $a;

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  Get item id by tied VariationId
     *
     *  @param int $id
     *  @return ItemEntity
     *  @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByVariationId(int $id) : ItemEntity {

        $e = new ItemEntity();

        $query = "SELECT * FROM ItemVariations ".
            "LEFT JOIN Items ON Items.id = ItemVariations.parent_id ".
            "LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE ItemVariations.parent_id = ".(int)$id;

        $r = $this->selectBegin($query);

        while ($row = $this->fetchRow($r)){

            $this->_assembleEntity($e,$row);

        }

        return $e;

    }


    /** ---------------------------------------------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param string|null $color
     *  @param string|null $size
     *  @param string|null $style
     *  @return ItemEntity[]
     */
    public function getBySetOfStringParameters(string $color = null, string $size = null, string $style = null) : array {

        $uid = WorkingSet::getUserEntity()->getVendorId();

        /*
        $pa = [];
        $pa[] = isset($color) ? "SELECT color_id, null as size_id, null as item_vid FROM Colors WHERE sysven = ".$uid." AND color_name = '".StringsHelper::escapeInput($color)."'" : null;
        $pa[] = isset($size) ? "SELECT null as color_id, size_id, null as item_vid FROM Sizes WHERE sysven = ".$uid." AND size_name = '".StringsHelper::escapeInput($size)."'" : null;
        $pa[] = isset($style) ? "SELECT null as color_id, null as size_id, item_vid FROM ItemStyles WHERE sysvid = ".$uid." AND style_code = '".StringsHelper::escapeInput($style)."'" : null;

        $match = 0;

        $query = "";

        foreach ($pa as $v) {
            if(!empty($v)){

                if($match == 0) {
                    $query .= $v;
                } else {
                    $query .= " UNION ".$v;
                }
                $match++;
            }
        }
        */

        // -----------------------------------------------------------------------------
        // Switched for using store procedure instead of default query string assembling
        // -----------------------------------------------------------------------------
        $query = "CALL select_iv_by_params(".$uid.",'".(isset($color) ? StringsHelper::escapeInput($color) : '')."','"
            .(isset($size) ? StringsHelper::escapeInput($size) : '')."','"
            .(isset($style) ? StringsHelper::escapeInput($style) : '')."');";

        $r = $this->selectBegin($query);

        $colors = [];
        $sizes = [];
        $styles = [];

        while ($row = $this->fetchRow($r)) {

            if(isset($row['color_id'])) {
                $colors[] = $row['color_id'];
            }

            if(isset($row['size_id'])) {
                $sizes[] = $row['size_id'];
            }

            if(isset($row['item_id'])) {
                $styles[] = $row['item_id'];
            }

        }

        $r->closeCursor();
        $whereString = "";
        $found = 0;
        $need = 0;

        if(count($colors) > 0){
            $whereString .= " AND ItemVariations.color_id IN (".implode(',',$colors).")";
            $found++;
            $need++;
        } elseif (isset($color)) $need++;

        if(count($sizes) > 0) {
            $whereString .= " AND ItemVariations.size_id IN (".implode(',',$sizes).")";
            $found++;
            $need++;
        } elseif (isset($size)) $need++;

        if(count($styles) > 0) {
            $whereString .= " AND Items.id IN (".implode(',',$styles).")";
            $found++;
            $need++;
        } elseif (isset($style)) $need++;

        $a = [];

        if($found == $need) {

            $query = "SELECT * FROM ItemVariations " .
                "LEFT JOIN Items ON Items.id = ItemVariations.parent_id " .
                "LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId " .
                "WHERE ItemVariations.sysvid = " . $uid . $whereString . " LIMIT 1024";

            $colors = null;
            $sizes = null;
            $styles = null;
            $whereString = null;

            $r = $this->selectBegin($query);


            $i = 0;

            $itemid = null;
            $e = new ItemEntity();

            while ($row = $this->fetchRow($r)) {

                if ($itemid != $row['id']) {
                    $ent = new ItemEntity();
                    $a[$i] = $ent;
                    $e = &$a[$i++];
                }

                $this->_assembleEntity($e, $row);
                $itemid = $row['id'];
            }

            $r->closeCursor();
        }

        return $a;

    }


    /** ---------------------------------------------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param int|null $color
     *  @param int|null $size
     *  @return ItemEntity[]
     */
    public function getBySetOfNumParameters(int $color = null, int $size = null) : array {

        $whereString = "";

        if(isset($color)){
            $whereString .= " AND ItemVariations.color_id = ".$color;
        }
        if(isset($size)) {
            $whereString .= " AND ItemVariations.size_id = ".$size;
        }

        $e = new ItemEntity();

        $query = "SELECT * FROM ItemVariations ".
            "LEFT JOIN Items ON Items.id = ItemVariations.parent_id ".
            "LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE ItemVariations.sysvid = ".WorkingSet::getUserEntity()->getVendorId().$whereString;

        $r = $this->selectBegin($query);

        $a = [];
        $i = 0;

        $itemid = null;

        while ($row = $this->fetchRow($r)) {

            if($itemid != $row['id']) {
                $ent = new ItemEntity();
                $a[$i] = $ent;
                $e = &$a[$i++];
            }

            $this->_assembleEntity($e,$row);
            $itemid = $row['id'];

        }

        return $a;

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param int $id
     *  @return ItemEntity
     *  @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getSingleItemByVariationId(int $id) : ItemEntity {

        $query = "SELECT * FROM Items WHERE id=(SELECT parent_id FROM ItemVariations WHERE var_id = ".$id.");";

        $r = $this->selectBegin($query);

        if($row = $this->fetchRow($r)) {

            $e = $this->_assembeSingleEntity($row);

        } else {
            $e = new ItemEntity();
        }

        return $e;

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *                                 Assemble Item only entity
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param array $a
     *  @return ItemEntity
     */
    private function _assembeSingleEntity(array $a) {

        $e = new ItemEntity();
        $e->setId($a['id']);
        $e->setOwnerId((int)$a['sysvid']);
        $e->setCustomId($a['custom_id']);
        $e->setTitle($a['title']);
        $e->getBrand()->setId($a['brand']);

        if($styles = json_decode($a['styles'],true))
            $e->getStyle()->fromArray($styles);

        if(isset($a['info']))
            $e->getInformationObject()->fromArray(json_decode($a['info'],true));

        if($cats = json_decode($a['catalog'], true)) {
            $e->getCategoryObject()->fromArray($cats);
            $e->setCategory($e->getCategoryObject()->getCategoryLast());
        }

        if(isset($a['description']))
            $e->getDescObject()->fromArray(json_decode($a['description'],true));

        return $e;

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *                                  Assemble single entity and it's parent
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param ItemEntity $e
     *  @param array      $a
     */
    private function _assembleEntity(ItemEntity &$e, array &$a) {

        try {

            if (empty($e->getId())) {

                $e->setId($a['id']);
                $e->setOwnerId((int)$a['sysvid']);
                $e->setCustomId($a['custom_id']);
                $e->setTitle($a['title']);
                $e->getBrand()->setId($a['brand']);
                $e->getBrand()->setName($a['brand_name']);
                if ($styles = json_decode($a['styles'], true))
                    $e->getStyle()->fromArray($styles);

                if (isset($a['info']))
                    $e->getInformationObject()->fromArray(json_decode($a['info'], true));

                if ($cats = json_decode($a['catalog'], true)) {
                    $e->getCategoryObject()->fromArray($cats);
                    $e->setCategory($e->getCategoryObject()->getCategoryLast());
                }

                if (isset($a['description']))
                    $e->getDescObject()->fromArray(json_decode($a['description'], true));

            }

            $var = new ItemVariationEntity();

            $var->setId((int)$a['var_id']);
            $var->setCustomId($a['custom_vid']);
            $var->setParentId((int)$a['parent_id']);
            $var->setOwnerId((int)$a['sysvid']);

            $var->getSize()->setId((int)$a['size_id']);
            $var->getSize()->setName($a['size_name']);
            $var->getColor()->setId((int)$a['color_id']);
            $var->getColor()->setName($a['color_name']);

            $var->setCode($a['code']);
            $var->setWeight((float)$a['weight']);
            $var->setHeight((float)$a['height']);
            $var->setWidth((float)$a['width']);
            $var->setPrice((float)$a['price']);

            if ($attrs = json_decode($a['attributes'], true))
                $var->getAttributes()->fromArray($attrs);
            if ($vdata = json_decode($a['vardata'], true))
                $var->getInformationObject()->fromArray($vdata);

            $e->setVariation($var);

        } catch (\Error $e) {

            /*
             *  If any errors Happened during DB entity assemble, ignore them
             *  Data inconsistency is a problem of a last stage of development.
             *
             *  If error happen — no entity will be populated
             */

        }

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *                                      GET ITEM CACHED OBJECT FROM DATABASE
     *
     *                      Retrieving from database item without any variation objects attached
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param int[] $idList
     *  @return ItemEntity[]
     *  @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getItemsCachedObject(array $idList) {

        $query = "SELECT * FROM Items WHERE id IN (".implode(',',$idList).")";

        $r = $this->selectBegin($query);

        $a = [];

        if($r) {

            while ($row = $this->fetchRow($r)) {

                $e = new ItemEntity();
                $e->setId($row['id']);
                $e->setOwnerId((int)$row['sysvid']);
                $e->setCustomId($row['custom_id']);
                $e->setTitle($row['title']);
                $e->getBrand()->setId($row['brand']);

                if($styles = json_decode($row['styles'],true))
                    $e->getStyle()->fromArray($styles);

                if(isset($row['info']))
                    $e->getInformationObject()->fromArray(json_decode($row['info'],true));

                if($cats = json_decode($row['catalog'], true)) {
                    $e->getCategoryObject()->fromArray($cats);
                    $e->setCategory($e->getCategoryObject()->getCategoryLast());
                }

                if(isset($row['description']))
                    $e->getDescObject()->fromArray(json_decode($row['description'],true));

                $a[] = $e;

            }

        }

        return $a;

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *                                  SET ITEM CACHED OBJECT OT DATABASE
     *
     *          Setting only Item Object back to DB. No Variations involved in this process so
     *          changes will be affected only for items itself
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param ItemEntity[] $items
     */
    public function setItemsCachedObject(array &$items) {

        $query = "";

        foreach ($items as $i) {

            $cat = (!empty($i->getCategory()->getId())) ? $i->getCategory()->getId() : 0;

            $query .= "UPDATE Items SET brand='".$i->getBrand()->getId()
                ."',category='".$cat
                ."',title='".StringsHelper::escapeInput($i->getTitle())
                ."',info='".StringsHelper::escapeJson($i->getInformationObject()->toJson())
                ."',description='".StringsHelper::escapeJson($i->getDescObject()->toJson())
                ."',catalog='".StringsHelper::escapeJson($i->getCategoryObject()->toJson())
                ."',styles='".StringsHelper::escapeJson($i->getStyle()->toJson())
                ."',custom_id='".StringsHelper::escapeJson($i->getCustomId())
                ."' WHERE id = ".$i->getId().";";

        }

        $this->writeBegin($query);

    }

    /** ---------------------------------------------------------------------------------------------------------------
     *
     *                                  SET ITEM INFO CACHED OBJECT TO DATABASE
     *
     *          Setting only Item Info Object back to DB. No other fields or variations will be processed
     *          with that request. Common usage is to update images or bullets.
     *
     *  ---------------------------------------------------------------------------------------------------------------
     *
     *  @param ItemEntity[] $items
     */
    public function setItemsCachedInfoObject(array &$items)  {

        $query = "";

        foreach ($items as $i) {

            $query .= "UPDATE Items SET info='".
                StringsHelper::escapeJson($i->getInformationObject()->toJson())
                ."' WHERE id = ".$i->getId().";";

        }

        $this->writeBegin($query);

    }

}