<?php namespace App\Model\ItemModel;

use App\Model\ItemAbstract\ItemAbstractFactory;
use App\Model\ItemCategoryModel\ItemCategoryFactory;

class ItemFactory extends ItemAbstractFactory
{

    public static function createOneFromJson(string $jsonString) : ItemEntity {

        // Check if JSON dictionary is came to the system
        $type = ($jsonString[0] == '{') ? true : false;

        // Assemble single
        if($type) {

            // Decode to array
            if ($itemData = json_decode($jsonString, true)) {

                $item = self::assembleNewItem($itemData);

                // Check if categories exists
                if (isset($itemData['category'])) {

                    // Set category object entity to ItemCategoryEntity
                    $item->setCategoryObject(ItemCategoryFactory::createFromArray($itemData['category']));

                    // Detect if item category has an exact object of category
                    if($cat = $item->getCategoryObject()->getCategoryLast())
                        $item->setCategory($cat);

                }

                // Parse Info object of JSON Item Object
                if (isset($itemData['info'])) {

                    $item->setInformationObject(self::assembleInfoBlock($itemData['info']));

                }

                // If items has a variations then extract them to item object
                if (isset($itemData['variations'])) {

                    foreach ($itemData['variations'] as $v) {

                        $var = self::assembleNewVariation($v);
                        $item->setVariation($var);

                    }

                }

                return $item;

            }

            throw new \RuntimeException("Json data is corrupted", 400);

        }

        throw new \RuntimeException("This method require only single item object",400);

    }

    public static function createMultipleFromJson(string $jsonString) : array {

        // Check if JSON dictionary is came to the system
        $type = ($jsonString[0] == '[') ? true : false;

        // Assemble multiple
        if($type) {

            // Decode to array
            if ($itemData = json_decode($jsonString, true)) {




            }

            throw new \RuntimeException("Json data is corrupted",400);

        }

        throw new \RuntimeException("This method require array of objects", 400);

    }


}