<?php namespace App\Model\ItemAbstract;

use App\Model\BrandModel\BrandEntity;
use App\Model\ItemCommon\ItemDescriptionEntity;
use App\Model\ItemCommon\ItemImage;
use App\Model\ItemModel\ItemEntity;
use App\Model\ItemCommon\ItemInformationEntity;
use App\Model\ItemVariationModel\ItemVariationEntity;
use App\Model\ItemVariationModel\ItemVariationMetaEntity;

abstract class ItemAbstractFactory
{
    
    protected static $lastItemFieldList = [];
    protected static $lastVariationFieldList;
    
    /* ----------------------------------------------------------------------------------------------------------
     *
     *             METHODS RELATED TO ASSEMBLE ITEMS FROM NOT STRICT INCOMING JSON STRUCTURE
     *
     * --------------------------------------------------------------------------------------------------------*/

    /**
     * @param array $array
     * @return ItemEntity
     */
    protected static function assembleNewItem(array $array) : ItemEntity {

        if(isset($array['title'])) {

            $item = new ItemEntity();

            $item->setTitle($array['title']);

            if(isset($array['customId']))
                $item->setCustomId($array['customId']);

            if(isset($array['brand'])) {
                $b = new BrandEntity();
                $b->setName($array['brand']);
                $item->setBrandObject($b);
            }

            if(isset($array['description'])) {
                $item->setDescObject(self::assembleDescBlock($array['description']));
            }

            if (isset($array['style'])) {
                $item->getStyle()->fromArray($array['style']);
            }

            return $item;
        }

        $err = print_r($array,true);
        throw new \RuntimeException("Variation cant be assembled from this data: ".$err,400);

    }

    /**
     * Array from json data
     * @param array $array
     *
     * Will return new assembled variation entity
     * @return ItemVariationEntity
     */
    protected static function assembleNewVariation(array $array) : ItemVariationEntity {
        
        self::$lastVariationFieldList = new ItemVariationMetaEntity();
        
        if(isset($array['color']) && isset($array['price']) ) {

            $ivar = new ItemVariationEntity();

            if(isset($array['id'])) {
                $ivar->setId((int)$array['id']);
            }

            if(isset($array['parentId'])) {
                $ivar->setParentId((int)$array['parentId']);
                self::$lastVariationFieldList->parentId = true;
            }

            if(isset($array['colorId']))
                $ivar->getColor()->setId((int)$array['colorId']);

            $ivar->getColor()->setName($array['color']);
            self::$lastVariationFieldList->color = true;

            $ivar->setPrice($array['price']);
            self::$lastVariationFieldList->price = true;
            
            if(isset($array['customId'])) {
                $ivar->setCustomId($array['customId']);
                self::$lastVariationFieldList->customId = true;
            }
            
            if(isset($array['sizeId'])) {
                $ivar->getSize()->setId($array['sizeId']);
            }

            if(isset($array['size'])) {
                $ivar->getSize()->setName($array['size']);
                self::$lastVariationFieldList->size = true;
            }

            if(isset($array['code'])) {
                $ivar->setCode($array['code']);
                self::$lastVariationFieldList->code = true;
            }

            if(isset($array['weight'])) {
                $ivar->setWeight((float)$array['weight']);
                self::$lastVariationFieldList->weight = true;
            }

            if(isset($array['height'])) {
                $ivar->setHeight((float)$array['height']);
                self::$lastVariationFieldList->height = true;
            }

            if(isset($array['width'])) {
                $ivar->setWidth((float)$array['width']);
                self::$lastVariationFieldList->width = true;
            }

            if (isset($array['info'])) {
                $ivar->getInformationObject()->fromArray($array['info']);
                self::$lastVariationFieldList->info = true;
            }

            if (isset($array['attributes'])) {
                $ivar->getAttributes()->fromArray($array['attributes']);
                self::$lastVariationFieldList->attributes = true;
            }

            return $ivar;

        }

        $err = print_r($array,true);
        throw new \RuntimeException("Variation cant be assembled from this data: ".$err,400);

    }

    /**
     * @param array $descBlock
     * @return ItemDescriptionEntity
     */
    protected static function assembleDescBlock(array &$descBlock) : ItemDescriptionEntity {

        $de = new ItemDescriptionEntity();

        foreach($descBlock as $n => $db) {

            if($n == "short") {

                $de->setShort($db);

            } elseif ($n == "full") {

                $de->setFull($db);

            } else {

                $de->setDescWithName($n,$db);

            }

        }

        return $de;

    }

    /**
     * @param array $infoBlock
     * @return ItemInformationEntity
     */
    protected static function assembleInfoBlock(array &$infoBlock) : ItemInformationEntity {

        $infoObject = new ItemInformationEntity();

        // Image block inside Info block
        if (isset($infoBlock['images'])) {

            $imagesBlock = &$infoBlock['images'];

            $name = isset($imagesBlock['titleImage']["name"]) ? $imagesBlock['titleImage']["name"] : "";

            // Put title image into info block object
            if (isset($imagesBlock['titleImage'])) {

                $id = isset($imagesBlock['titleImage']['id']) ? $imagesBlock['titleImage']['id'] : null;

                $infoObject->setItemTitleImage(
                    new ItemImage($id,
                        $imagesBlock['titleImage']["src"],
                        $name,
                        $imagesBlock['titleImage']["desc"]));

            }

            // Put image set
            if (isset($imagesBlock['imageSet'])) {

                foreach($imagesBlock['imageSet'] as $imageSource) {

                    $id = isset($imageSource['id']) ? $imageSource['id'] : null;
                    $name = isset($imageSource["name"]) ? $imageSource["name"] : "";
                    $infoObject->addItemImage(new ItemImage($id, $imageSource["src"], $name, $imageSource["desc"]));

                }

            }

        }

        if (isset($infoBlock['bullets'])) {

            foreach($infoBlock['bullets'] as $b) {
                $infoObject->addBullet($b);
            }
  
        }

        return $infoObject;

    }

}