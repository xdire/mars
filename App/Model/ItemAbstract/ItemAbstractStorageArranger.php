<?php namespace App\Model\ItemAbstract;

use App\Model\AttributeModel\AttributesDAO;
use App\Model\BrandModel\BrandDAO;
use App\Model\BrandModel\BrandEntity;
use App\Model\ColorModel\ColorDAO;
use App\Model\ColorModel\ColorEntity;
use App\Model\Current\WorkingCache;
use App\Model\ItemCategoryModel\ItemCategoryDAO;
use App\Model\ItemCategoryModel\ItemCategoryNode;
use App\Model\ItemImage\ItemImageDAO;
use App\Model\ItemImage\ItemImageS3Uploader;
use App\Model\ItemModel\ItemEntity;
use App\Model\ItemVariationModel\ItemVariationEntity;
use App\Model\SizeModel\SizeDAO;
use App\Model\SizeModel\SizeEntity;
use Xdire\Dude\Core\App;

abstract class ItemAbstractStorageArranger
{

    private $imageStorageConfig;

    function __construct()
    {
        $this->imageStorageConfig = App::getConfig('image_storage');
    }

    /**
     * @param ItemEntity $e
     */
    protected function _checkImages(ItemEntity &$e) {

        if($imgd = $e->getInformationObject()) {

            //$creds = $this->imageStorageConfig;

            $idao = new ItemImageDAO();

            //$uploader = new ItemImageS3Uploader($creds['keyId'],$creds['keySecret'],$creds['bucket']);
            $uploader = new ItemImageS3Uploader();
            $default = App::getConfig('defaultItemImage');

            if($imgt = $imgd->getItemTitleImage()) {

                if($imgcdata = WorkingCache::getImageFromCache($imgt)) {
                    $imgt = $imgcdata;
                } else {
                    $link = $imgt->getImageData();
                    $imgt->setImageData($default["src"]);
                    $imgt->setImageExt($default["ext"]);

                    $idao->save($imgt);

                    $imgt = $uploader->uploadItemImage($imgt,$link);


                    WorkingCache::addImageToCache($imgt,$link,0);
                }

                $imgd->setItemTitleImage($imgt);

            }

            if($imgs = $imgd->getItemImagesSet()) {

                foreach($imgs as &$img) {

                    if($imgcdata = WorkingCache::getImageFromCache($img)) {
                        $img = $imgcdata;
                    } else {
                        $link = $img->getImageData();
                        $img->setImageData($default["src"]);
                        $img->setImageExt($default["ext"]);
                        $idao->save($img);

                        $img = $uploader->uploadItemImage($img,$link);
                        WorkingCache::addImageToCache($img,$link,0);
                    }

                }

            }
            
            $imgd->setItemImagesSet($imgs);

        }

    }

    /**
     * @param ItemEntity $e
     */
    protected function _checkCategory(ItemEntity &$e) {

        if($cat = $e->getCategoryObject()) {

            $cdao = new ItemCategoryDAO();

            if($top = $cat->getCategoryTop()) {

                $tree = $cdao->getTreeByRootName($top->getName());

                $catItems = $tree->getCategoryDepth();

                // Get links to structural elements
                $catList = $cat->getList();
                $treeList = $tree->getList();

                // Walk down by Catalog structure along with Found Tree structure
                $lastParent = null;

                while($catItems-- > 0) {

                    /** @var ItemCategoryNode $currentCat */
                    $currentCat = $catList->current();

                    /** @var ItemCategoryNode $currentTree */
                    $currentTree = $treeList->current();

                    // If Current Tree is still matching given Cat structure - assign ID
                    if(isset($currentTree) && isset($currentCat)) {
                        
                        if (($currentCat->getName() == $currentTree->getName()) && ($lastParent == $currentTree->getParentId())) {

                            $currentCat->setId($currentTree->getId());
                            $lastParent = $currentTree->getId();
                            $catList->next();

                        }

                    }

                    $treeList->next();

                }

            }

            // Try to save category if needed, DAO will save only what it's need to save
            $cdao->save($cat);

        }

    }

    /**
     * @param ItemEntity $e
     */
    protected function _checkBrand(ItemEntity &$e) {

        if($brand = $e->getBrand()) {

            $bdao = new BrandDAO();

            // Select Brand Entity by existing properties
            if($id = $brand->getId()) {
                $bentity = $bdao->getById($id);
            }
            elseif ($name = $brand->getName()) {
                $bentity = $bdao->getByName($name);
            }
            else
                $bentity = new BrandEntity();

            // If Brand entity was found for current brand then add it's id to ItemEntity
            if(!$bentity->isEmpty()) {

                $e->setBrandObject($bentity);

            }
            // If nothing was found then create new Brand entity and attach to ItemEntity
            else {

                $bentity = $bdao->save($brand);
                $e->setBrandObject($bentity);

            }

        }

    }

    /**
     * @param ItemVariationEntity $e
     */
    protected function _checkVariatonColor(ItemVariationEntity &$e) {

        if($color = $e->getColor()) {

            $bdao = new ColorDAO();

            // Select
            if($id = $color->getId()) {
                $centity = $bdao->getById($id);
            }
            elseif ($name = $color->getName()) {
                $centity = $bdao->getByName($name);
            }
            else
                $centity = new ColorEntity("");

            // If
            if(!$centity->isEmpty()) {

                $e->setColorObject($centity);

            }
            // If nothing was found
            else {

                $e->setColorObject($bdao->save($color));

            }

        }

    }

    /**
     * @param ItemVariationEntity $e
     */
    protected function _checkVariationSize(ItemVariationEntity &$e) {

        if($size = $e->getSize()) {

            $sdao = new SizeDAO();

            // Select
            if($id = $size->getId()) {
                $centity = $sdao->getById($id);
            }
            elseif ($name = $size->getName()) {
                $centity = $sdao->getByName($name);
            }
            else
                $centity = new SizeEntity("");

            // If
            if(!$centity->isEmpty()) {

                $e->setSizeObject($centity);

            }
            // If nothing was found
            else {

                $e->setSizeObject($sdao->save($size));

            }

        }

    }

    /**
     * @param ItemVariationEntity $e
     */
    protected function _checkVariationInfo(ItemVariationEntity &$e) {

        if($imgd = $e->getInformationObject()) {

            //$creds = $this->imageStorageConfig;

            $idao = new ItemImageDAO();

            //$uploader = new ItemImageS3Uploader($creds['keyId'],$creds['keySecret'],$creds['bucket']);
            $uploader = new ItemImageS3Uploader();
            $default = App::getConfig('defaultItemImage');

            if($imgt = $imgd->getItemTitleImage()) {

                if($imgcdata = WorkingCache::getImageFromCache($imgt)) {
                    $imgt = $imgcdata;
                } else {
                    $link = $imgt->getImageData();
                    $imgt->setImageData($default["src"]);
                    $imgt->setImageExt($default["ext"]);

                    $idao->save($imgt);

                    $imgt = $uploader->uploadItemImage($imgt,$link);

                    WorkingCache::addImageToCache($imgt,$link,0);
                }
                
                $imgd->setItemTitleImage($imgt);

            }

            if($imgs = $imgd->getItemImagesSet()) {

                foreach($imgs as &$img) {

                    if($imgcdata = WorkingCache::getImageFromCache($img)) {
                        $img = $imgcdata;
                    } else {
                        $link = $img->getImageData();
                        $img->setImageData($default["src"]);
                        $img->setImageExt($default["ext"]);
                        $idao->save($img);

                        $img = $uploader->uploadItemImage($img,$link);
                        WorkingCache::addImageToCache($img,$link,0);
                    }

                }

            }

            $imgd->setItemImagesSet($imgs);

        }

    }

    /**
     * Attributes belongs to Variation
     *
     * @param ItemVariationEntity $e
     */
    protected function _checkVariationAttributes(ItemVariationEntity &$e) {

        if($attr = $e->getAttributes()) {

            $adao = new AttributesDAO();

            $adao->matchSetOfAttributesByObject($attr);

            $adao->save($attr);

        }

    }

}