<?php namespace App\Model\ItemAbstract;

/**
 * Class ItemAbstractImage
 *
 * Abstract image
 *
 * @package App\Model\ItemAbstract
 */
abstract class ItemAbstractImage
{
    /** int */
    const TYPE_LINK = 0;
    /** int */
    const TYPE_BASE64 = 1;

    /** @var int */
    protected $typeOfImage = 0;
    /** @var string */
    protected $imageData;
    /** @var  string */
    protected $ext;

    public function detectImageTypeOfData() : int {

        if(strpos($this->imageData,"://") !== false){
            return 0;
        } else {
            return 1;
        }

    }

    public function getTypeOfImageData() : int {

        return $this->typeOfImage;

    }

}