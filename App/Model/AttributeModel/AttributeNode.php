<?php namespace App\Model\AttributeModel;

class AttributeNode
{
    /** @var int | null */
    private $id;
    /** @var string | null */
    private $name;
    /** @var int | null */
    private $typeId;
    /** @var string | null */
    private $type;

    /**
     * AttributeNode constructor.
     * @param $id
     * @param $name
     * @param $type
     * @param $typeId
     */
    public function __construct($id, $name, $type, $typeId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->typeId = $typeId;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param null|string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int|null
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param int|null $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }

    /**
     * @return array
     */
    public function toArray() : array {

        return ["id" => $this->id, "name" => $this->name];

    }

}