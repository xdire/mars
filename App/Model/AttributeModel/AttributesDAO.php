<?php namespace App\Model\AttributeModel;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use Xdire\Dude\Core\DB\DBDuplicationException;
use Xdire\Dude\Core\DB\DBO;

class AttributesDAO extends DBO
{

    /**
     * @param AttributesEntity $e
     * @return AttributesEntity
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    function matchSetOfAttributesByObject(AttributesEntity &$e) : AttributesEntity {

        $attributes = [];
        $attrTypes = [];
        $attrcache = [];

        $curType = null;

        /** @var AttributeNode $attr */
        foreach ($e->getAttributes() as &$attr) {
            $attributes[] = '"'.$attr->getName().'"';
            if($curType != $attr->getType())
                $attrTypes[] = '"'.$attr->getType().'"';
            $attrcache[$attr->getName().$attr->getType()] = &$attr;
        }

        if(count($attributes) > 0) {

            $query = "SELECT attr_id,parent_id,attr_name,atyp_id,atyp_name FROM Attributes JOIN AttrTypes ON Attributes.parent_id = AttrTypes.atyp_id " .
                "WHERE Attributes.sysvid = " . WorkingSet::getUserEntity()->getVendorId() . " AND Attributes.attr_name IN (" . StringsHelper::escapeJson(implode(",", $attributes)) . ") " .
                "UNION SELECT NULL AS attr_id,NULL AS parent_id,NULL AS attr_name,atyp_id,atyp_name FROM AttrTypes " .
                "WHERE AttrTypes.sysvid = " . WorkingSet::getUserEntity()->getVendorId() . " AND " .
                "atyp_name IN (" . StringsHelper::escapeJson(implode(",", $attrTypes)) . ");";

            $r = $this->selectBegin($query);

            while ($row = $this->fetchRow($r)) {

                if($attr = &$attrcache[$row['attr_name'].$row['atyp_name']]) {

                    $attr->setId($row['attr_id']);
                    $attr->setTypeId($row['parent_id']);
                    $attr->setName($row['attr_name']);
                    $attr->setType($row['atyp_name']);

                }
                
                $e->setIdForIdentifiedType($row['atyp_name'],$row['atyp_id']);

            }

            $r->closeCursor();

        }

        return $e;

    }

    /**
     * @param AttributesEntity $e
     * @return AttributesEntity
     * @throws \Xdire\Dude\Core\DB\DBDuplicationException
     * @throws \Xdire\Dude\Core\DB\DBWriteException
     */
    function save(AttributesEntity &$e) : AttributesEntity {

        $uid = WorkingSet::getUserEntity()->getVendorId();

        /** @var AttributeNode $attr */
        foreach ($e->getAttributes() as &$attr) {

            if(empty($attr->getId())){

                // Check for Type Attribute is created and functional
                // create if needed
                if(!($typeId = $e->getIdForIdentifiedType($attr->getType()))){

                    try {

                        $query = "INSERT INTO AttrTypes (sysvid, atyp_name) VALUES (" . $uid . ",'" . StringsHelper::escapeInput($attr->getType()) . "')";
                        $this->writeBegin($query);
                        $typeId = $this->getLastInsertId();

                    } catch (DBDuplicationException $de) {

                        $query = "SELECT * FROM AttrTypes WHERE sysvid = ".$uid." AND atyp_name = '".StringsHelper::escapeInput($attr->getType())."'";
                        $r = $this->selectBegin($query);
                        if ($row = $this->fetchRow($r)) {
                            $typeId = $row['atyp_id'];
                        }

                    }

                    $attr->setTypeId($typeId);

                }

                // Add to Type Attribute new corresponding attribute
                $query = "INSERT INTO Attributes (sysvid, parent_id, attr_name) VALUES (".$uid.",".$typeId.",'".StringsHelper::escapeInput($attr->getName())."')";
                $this->writeBegin($query);
                $attr->setId($this->getLastInsertId());

            }

        }

        return $e;

    }

}