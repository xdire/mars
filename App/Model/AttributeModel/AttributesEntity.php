<?php namespace App\Model\AttributeModel;

class AttributesEntity
{
    /** @var int[] */
    private $identifiedTypes = [];
    /** @var array[] */
    private $attributes = [];

    /**
     * @param string      $type
     * @param string      $attribute
     * @param int|null    $id
     * @param string|null $typeName
     * @param int|null    $typeId
     */
    public function addAtributeOfType(string $type, string $attribute, int $id = null, string $typeName = null, int $typeId = null) {

        if(!isset($this->attributes[$type])) {
            $this->attributes[$type] = [];
        }

        $this->attributes[$type][] = new AttributeNode($id,$attribute,$typeName,$typeId);

    }

    /**
     * @param string $type
     * @param int    $id
     */
    public function setIdForIdentifiedType(string $type, int $id) {
        $this->identifiedTypes[$type] = $id;
    }

    /**
     * @param string $type
     * @return int|null
     */
    public function getIdForIdentifiedType(string $type) {
        if(isset($this->identifiedTypes[$type])){
            return $this->identifiedTypes[$type];
        }
        return null;
    }

    /**
     * @return \Generator|AttributeNode[]
     */
    public function &getAttributes() {

        /**
         * @var string $type
         * @var AttributeNode[] $attributes
         */
        foreach ($this->attributes as $type => &$attributes) {

            /** @var AttributeNode $attr */
            foreach ($attributes as &$attr) {
                yield $attr;
            }

        }

    }

    /**
     * @param string $type
     * @return AttributeNode[] | null
     */
    public function getAttributesOfType(string $type) {

        if(isset($this->attributes[$type])) {
            return $this->attributes[$type];
        }
        return null;

    }

    /**
     * @return string[]
     */
    public function getAllTypes() {
        return array_keys($this->attributes);
    }

    /**
     * @return array
     */
    public function toArray() : array {

        $r = [];
        $i = 0;

        foreach ($this->attributes as $t => $aa) {

            $r[$i] = [];
            $c = &$r[$i];

            /** @var AttributeNode $a */
            foreach ($aa as $a) {

                if ($id = $a->getTypeId())
                    $c['id'] = $a->getTypeId();

                $c['name'] = $a->getType();
                $c['attr'][] = $a->toArray();

            }

            $i++;

        }

        return $r;

    }

    /**
     * @return string
     */
    public function toJson() : string {
        return json_encode($this->toArray());
    }

    /**
     * @param array $a
     */
    public function fromArray(array $a) {

        foreach ($a as $av) {

            $tname = isset($av['name']) ? $av['name'] : null;
            $tid = isset($av['id']) ? $av['id'] : null;

            if(isset($av['attr'])) {

                foreach ($av['attr'] as $e) {

                    $id = isset($e['id']) ? $e['id'] : null;

                    $this->addAtributeOfType($tname, $e['name'], $id, $tname, $tid);

                }

            }

        }

    }
    
}