<?php namespace App\Model\BrandModel;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use Xdire\Dude\Core\DB\DBO;

/**
 * Class BrandDAO
 * @package App\Model\BrandModel
 */
class BrandDAO extends DBO
{

    /**
     * @param BrandEntity $e
     * @return BrandEntity
     * @throws \Xdire\Dude\Core\DB\DBDuplicationException
     * @throws \Xdire\Dude\Core\DB\DBWriteException
     */
    public function save(BrandEntity &$e) : BrandEntity {

        $query = "INSERT INTO Brands (sysvid, brand_name, brand_real) ".
            "VALUES (".WorkingSet::getUserEntity()->getVendorId().",'".
            StringsHelper::escapeInput($e->getName())."','".
            StringsHelper::escapeInput($e->getRealName())."');";

        $r = $this->writeBegin($query);

        if($this->getLastInsertId() > 0)
            $e->setId($this->getLastInsertId());

        return $e;

    }

    /**
     * @param BrandEntity $e
     * @return BrandEntity
     * @throws \Xdire\Dude\Core\DB\DBDuplicationException
     * @throws \Xdire\Dude\Core\DB\DBWriteException
     */
    public function update(BrandEntity &$e) : BrandEntity {

        $query = "UPDATE Brands SET ".
            "brand_name='".$e->getName().
            "', brand_real='".$e->getRealName().
            "' WHERE brand_id = ".$e->getId().";";

        $r = $this->writeBegin($query);

        return $e;

    }

    /**
     * @param string $name
     * @return BrandEntity
     * @throws \Xdire\Dude\Core\DB\DBNotFoundException
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByName(string $name) : BrandEntity {

        $be = new BrandEntity();

        $query = "SELECT * FROM Brands WHERE sysvid = ".
            WorkingSet::getUserEntity()->getVendorId().
            " AND brand_name = '".StringsHelper::escapeInput($name)."'";

        $r = $this->selectBegin($query);

        if($d = $this->fetchRow($r)){

            $be->setId($d['brand_id']);
            $be->setName($d['brand_name']);
            $be->setRealName($d['brand_real']);

        }

        return $be;

    }

    /**
     * @param int $id
     * @return BrandEntity
     * @throws \Xdire\Dude\Core\DB\DBNotFoundException
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getById(int $id) : BrandEntity {

        $be = new BrandEntity();

        $query = "SELECT * FROM Brands WHERE brand_id = ".
            (int)$id." AND sysvid = ".WorkingSet::getUserEntity()->getVendorId();

        $r = $this->selectBegin($query);

        if($d = $this->fetchRow($r)) {

            $be->setId($d['brand_id']);
            $be->setName($d['brand_name']);
            $be->setRealName($d['brand_real']);

        }

        return $be;

    }

}