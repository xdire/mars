<?php namespace App\Model\BrandModel;

/**
 * Class BrandEntity
 *
 * @package App\Model\BrandModel
 */
class BrandEntity
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $realName = "";

    /**
     * @return bool
     */
    public function isEmpty() : bool {
        return isset($this->id) ? false : true;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getRealName()
    {
        return $this->realName;
    }

    /**
     * @param string $realName
     */
    public function setRealName($realName)
    {
        $this->realName = $realName;
    }

}