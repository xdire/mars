<?php namespace App\Model\ItemImage;

class ItemImageIQUploader
{

    const LOCAL_CONNECTION = 1;
    const REMOTE_CONNECTION = 2;

    private $_host;
    private $_port;
    private $_type;
    private $_key;

    private $socket;

    private $connected = false;

    /**
     * ItemImageIQUploader constructor.
     * @param $host
     * @param $port
     * @param $type
     */
    function __construct($host, $port, $type, $key = null)
    {

        $this->_host = $host;
        $this->_port = $port;
        $this->_key = $key;
        $this->_type = $type;
        
        if($this->_type == self::LOCAL_CONNECTION) {
            $this->createLocalConnection();
        } else {
            $this->createRemoteConnection();
        }

    }

    function __destruct()
    {
        if($this->socket !== null){
            socket_close($this->socket);
        }
    }

    /**
     * @return boolean
     */
    public function isConnected()
    {
        return $this->connected;
    }

    /**
     * @param IQUploaderPackage $p
     * @throws \RuntimeException
     * @return IQUploaderMessage
     */
    public function sendPackage(IQUploaderPackage $p) {

        if($this->connected){
            socket_write($this->socket,$p->toJson(),$p->getLength());
            $input = socket_read($this->socket, 1024);
            $msg = new IQUploaderMessage($input);

            if($msg->isValid()){
                return $msg;
            }
        }

        throw new \RuntimeException("Can't upload image with IQUE",500);

    }

    /**
     * @param string $src
     * @param int    $imageId
     * @param int    $vendorId
     * @return IQUploaderPackage
     */
    public function createImagePackage(string $src, int $imageId, int $vendorId) : IQUploaderPackage {

        return new IQUploaderPackage(IQUploaderPackage::COMMAND_GETIMAGE,$src,$imageId,$vendorId);

    }

    /**
     *
     */
    public function createLocalConnection() {

        $this->socket = socket_create(AF_UNIX,SOCK_STREAM,0);

        if($this->socket !== false)
        {
            if(socket_connect($this->socket, $this->_host) !== false) {
                $this->connected = true;
            }
        }

    }

    public function createRemoteConnection() {

    }

}