<?php namespace App\Model\ItemImage;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use App\Model\ItemCommon\ItemImage;
use Xdire\Dude\Core\DB\DBO;

class ItemImageDAO extends DBO
{

    public function save(ItemImage &$i) : ItemImage {

        $query = "INSERT INTO Images (sysven, image_link, image_ext, image_desc) ".
            "VALUES (".WorkingSet::getUserEntity()->getVendorId().",'".$i->getImageData()."','".$i->getImageExt()."','".StringsHelper::escapeInput($i->getDescription())."')";

        $this->writeBegin($query);

        if($this->getLastInsertId() > 0)
            $i->setId($this->getLastInsertId());

        return $i;

    }

    /**
     * @param int[] $idRange
     * @return ItemImageLinkedStorage
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByIdRange(array $idRange) {

        $query = "SELECT * FROM ItemImages JOIN Images ON ItemImages.image_id = Images.image_id WHERE ItemImages.image_id IN (".implode(',',$idRange).")";

        $r = $this->selectBegin($query);
        
        $l = new ItemImageLinkedStorage();
        
        if($r) {

            while ($row = $this->fetchRow($r)) {
                $l->addComplexLinkage(
                    $row['item_id'],
                    $row['item_vid'],
                    $row['image_id'],
                    $row['image_link'],
                    $row['image_ext'],
                    $row['image_desc']);
            }

        }

        return $l;

    }

}