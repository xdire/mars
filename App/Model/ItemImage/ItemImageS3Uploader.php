<?php namespace App\Model\ItemImage;

use App\Model\Current\WorkingCache;
use App\Model\Current\WorkingSet;
use App\Model\ItemCommon\ItemImage;
use Aws\S3\S3Client;

class ItemImageS3Uploader
{
    ///** @var S3Client  */
    //private $connector;
    //private $bucketName;

    /**
     *  ItemImageS3Uploader constructor.
     */
    function __construct(){}
    /*function __construct(string $accessKey, string $secretKey, string $bucketName) {

        $this->connector = S3Client::factory([
            "credentials" => [
                "key" => $accessKey,
                "secret" => $secretKey
            ]
        ]);
        $this->bucketName = $bucketName;

    }*/

    /** -----------------------------------------------------------------
     *
     *  -----------------------------------------------------------------
     *
     */
    /*function createBucket() {

        $this->connector->createBucket(array('Bucket' => 'mybucket'));

    }*/

    /** -----------------------------------------------------------------
     *
     *  -----------------------------------------------------------------
     *  Upload image from ItemImage Object
     *
     *  @param ItemImage $i
     *  @return ItemImage|null
     */
    function uploadItemImage(ItemImage &$i,$link) {

        /*  --------------------------  \
         *  LINK BASED IMAGE UPLOADING
         *  -------------------------- */
        if($i->getTypeOfImageData() == $i::TYPE_LINK) {

            // Download
            $ique = WorkingSet::getIQUEService();
            // Create package
            $package = $ique->createImagePackage($link,$i->getId(),WorkingSet::getUserEntity()->getVendorId());
            // Send package
            $message = $ique->sendPackage($package);

            $i->setImageData($message->getLink());
            $i->setImageExt($message->getExtension());
            $i->__setAsInBucket();
            //$i->setFileName($name);

        }
        /*  --------------------------  \
         *    BINARY IMAGE UPLOADING
         *  -------------------------- */
        elseif ($i->getTypeOfImageData() == $i::TYPE_BASE64) {



        }

        return $i;

    }

    /**
     *  Get bytes of some image from specified link
     *
     *  @param string $link
     *  @return null|resource
     */
    /*private function _getImageFromLink(string $link) {

        $curl = curl_init($link);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_BINARYTRANSFER,true);

        $result = curl_exec($curl);

        if($result) {

            $data = fopen("php://temp/maxmemory:10485760", 'rb+');
            fputs($data, $result);
            rewind($data);

            return $data;

        }

        return null;

    }*/

    /** -----------------------------------------------------------------
     *
     *  -----------------------------------------------------------------
     *  Upload image from ItemImage Object
     *
     *  @param ItemImage $i
     *  @return ItemImage|null
     */
    /*
    function uploadItemImageOld(ItemImage &$i) {

        if($i->getTypeOfImageData() == $i::TYPE_LINK) {

            // Lookup in cache
            // If Cache lookup is true - finish operation and
            // merge cached item to $i ref
            if($imgcdata = WorkingCache::getImageFromCache($i)){

                $i = $imgcdata;

            }
            // Download
            elseif($stream = $this->_getImageFromLink($i->getImageData())){

                // Prepare
                if(($name = $i->getFileName()) == "") {
                    $name = $i->__generateFileName();
                } else {
                    $name = $name.($i->__generateTimestamp());
                }

                // Send
                $result = $this->connector->putObject(array(
                    'Bucket' => $this->bucketName."/v".WorkingSet::getUserEntity()->getVendorId(),
                    'Key'    => $name,
                    'Body'   => $stream
                ));

                // Update
                if($imgdata = $result->get('ObjectURL')) {

                    $oldName = $i->getImageData();

                    $i->setImageData(urldecode($imgdata));
                    $i->__setAsInBucket();
                    $i->setFileName($name);

                    // Set to cache
                    WorkingCache::addImageToCache($i,$oldName,0);

                }

                // Clean
                $stream = null;

            }

        }
        elseif ($i->getTypeOfImageData() == $i::TYPE_BASE64) {



        }

        return $i;

    }*/

}