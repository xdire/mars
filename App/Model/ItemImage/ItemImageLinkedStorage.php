<?php namespace App\Model\ItemImage;

use App\Model\ItemCommon\ItemImage;

class ItemImageLinkedStorage
{

    private $itemImages = [];

    private $itemIdList = [];

    private $itemVariationList = [];

    public function addItemImage(int $id, string $link, string $ext, string $desc) {
        $this->itemImages[$id] = new ItemImage($id, $link, "", $desc, $ext);
    }

    public function addItemIdLinkage(int $id, int $imgId) {
        if(!isset($this->itemIdList[$id]))
            $this->itemIdList[$id] = [];
        $this->itemIdList[$id][] = $imgId;
    }

    public function addVariationIdLinkage(int $id, int $imgId) {
        if(!isset($this->itemVariationList[$id]))
            $this->itemVariationList[$id] = [];
        $this->itemVariationList[$id][] = $imgId;
    }

    public function addComplexLinkage(
        int $itemId, int $variationId, int $imageId, string $link, string $ext, string $desc) {
        if($itemId > 0) {
            if (!isset($this->itemIdList[$itemId]))
                $this->itemIdList[$itemId] = [];
            $this->itemIdList[$itemId][] = $imageId;
        }
        if($variationId > 0) {
            if (!isset($this->itemVariationList[$variationId]))
                $this->itemVariationList[$variationId] = [];
            $this->itemVariationList[$variationId][] = $imageId;
        }
        $this->itemImages[$imageId] = new ItemImage($imageId, $link, "", $desc, $ext);
    }

    /**
     * @param $id
     * @return ItemImage|null
     */
    public function getItemImageWithId(int $id) {
        if(isset($this->itemImages[$id])) {
            return $this->itemImages[$id];
        }
        return null;
    }

    /**
     * @return int[]
     */
    public function getItemIdLIst() : array {
        return array_keys($this->itemIdList);
    }

    /**
     * @return int[]
     */
    public function getVariationIdList() : array {
        return array_keys($this->itemVariationList);
    }

    /**
     * @param int $id
     * @return ItemImage[]
     */
    public function getImagesForItemId(int $id) : array {

        $a = [];

        if(isset($this->itemIdList[$id])) {

            foreach ($this->itemIdList[$id] as $iid){
                $a[] = $this->itemImages[$iid];
            }

        }

        return $a;

    }

    /**
     * @param int $id
     * @return ItemImage[]
     */
    public function getImagesForVariationId(int $id) : array {

        $a = [];

        if(isset($this->itemVariationList[$id])) {

            foreach ($this->itemVariationList[$id] as $iid){
                $a[] = $this->itemImages[$iid];
            }

        }

        return $a;

    }

}