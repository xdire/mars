<?php namespace App\Model\ItemImage;

class IQUploaderPackage
{
    const COMMAND_GETIMAGE = 1;
    const COMMAND_GETSYSPID = 8;
    const COMMAND_STOP = 9;

    private $command = "";
    private $source = "";
    private $imageId = null;
    private $vendorId = null;
    private $key = null;

    private $length = 0;

    private $_stringCmd = "";

    /**
     * IQUploaderPackage constructor.
     * @param int|null    $command
     * @param string|null $source
     * @param int|null    $imageId
     * @param int|null    $vendorId
     * @param string|null $key
     */
    public function __construct(int $command = null, string $source = null, int $imageId = null, int $vendorId = null, string $key = null)
    {
        $this->source = $source;
        $this->imageId = $imageId;
        $this->vendorId = $vendorId;

        $this->setCommand($command);
    }

    /**
     * @return array
     */
    public function toArray() : array {
        return ["cmd"=>$this->_stringCmd,"src"=>$this->source,"iid"=>$this->imageId,"vid"=>$this->vendorId,"key"=>$this->key];
    }

    /**
     * @return string
     */
    public function toJson() : string {
        $e = json_encode($this->toArray(), JSON_UNESCAPED_SLASHES);
        $this->length = strlen($e);
        return $e;
    }

    /**
     * @return int|string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param int|string $command
     */
    public function setCommand($command)
    {
        if($command == 1){
            $this->_stringCmd = "getimg";
        } elseif ($command == 8) {
            $this->_stringCmd = "proc";
        } elseif ($command == 9) {
            $this->_stringCmd = "stop";
        }

    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return int|null
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param int|null $imageId
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
    }

    /**
     * @return int|null
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param int|null $vendorId
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
    }

    /**
     * @return null
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param null $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

}