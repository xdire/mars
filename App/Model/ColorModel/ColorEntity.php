<?php namespace App\Model\ColorModel;

class ColorEntity
{

    /** @var int  */
    private $id;
    /** @var string  */
    private $name;
    /** @var string */
    private $real = "";

    /**
     * ColorEntity constructor.
     * @param string $name
     */
    function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool {
        return isset($this->id) ? false : true;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getRealName() : string
    {
        return $this->real;
    }

    /**
     * @param string $real
     */
    public function setRealName($real)
    {
        $this->real = $real;
    }

}