<?php namespace App\Model\ColorModel;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use Xdire\Dude\Core\DB\DBO;

class ColorDAO extends DBO
{

    /**
     * @param ColorEntity $e
     * @return ColorEntity
     * @throws \Xdire\Dude\Core\DB\DBWriteException
     */
    function save(ColorEntity &$e) : ColorEntity {

        $e->setName(StringsHelper::escapeInput($e->getName()));

        if($e->getRealName() == "")
            $e->setRealName($e->getName());

        $query = "INSERT INTO Colors ".
            "(sysven, color_name, color_real) ".
            "VALUES (".WorkingSet::getUserEntity()->getVendorId().",'".$e->getName()."','".$e->getRealName()."');";

        $this->writeBegin($query);

        if($this->getLastInsertId() > 0)
            $e->setId($this->getLastInsertId());

        return $e;

    }

    /**
     * @param string $name
     * @return ColorEntity
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    function getByName(string $name) : ColorEntity {

        $e = new ColorEntity($name);

        $query = "SELECT * FROM Colors ".
            "WHERE sysven = ".WorkingSet::getUserEntity()->getVendorId()." AND color_name = '".StringsHelper::escapeInput($name)."';";

        $r = $this->selectBegin($query);

        if($row = $this->fetchRow($r)){

            $e->setId($row['color_id']);
            $e->setName($row['color_name']);
            $e->setRealName($row['color_real']);

        }

        return $e;

    }

    /**
     * @param int $id
     * @return ColorEntity
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    function getById(int $id) : ColorEntity {

        $query = "SELECT * FROM Colors ".
            "WHERE color_id = ".$id." AND sysven = ".WorkingSet::getUserEntity()->getVendorId().";";

        $r = $this->selectBegin($query);

        $e = new ColorEntity("");

        if($row = $this->fetchRow($r)){

            $e->setId($row['color_id']);
            $e->setName($row['color_name']);
            $e->setRealName($row['color_real']);

        }

        return $e;

    }

}