<?php namespace App\Model\Helpers;

class StringsHelper
{

    static final function cleanInput($input) : string {
        return preg_replace('/[^a-zA-Z0-9\-_\s.&\(\)@]+/i','', $input);
    }

    static final function escapeInput($input) : string {

        $l = strlen($input);
        $n = "";

        for($i=0; $i < $l; $i++) {

            $a = $input[$i];

            switch ($a) {
                case '\\':
                    $n .= '\\\\';
                    break;
                case '\'':
                    $n .= '\\\'';
                    break;
                case '"':
                    $n .= '\"';
                    break;
                default:
                    $n .= $a;
            }

        }

        return $n;

    }

    static final function escapeJson($input) : string {

        $l = strlen($input);
        $n = "";

        for($i=0; $i < $l; $i++) {

            $a = $input[$i];

            switch ($a) {
                case '\\':
                    $n .= '\\\\';
                    break;
                case "'":
                    $n .= "\\'";
                    break;
                default:
                    $n .= $a;
            }

        }

        return $n;

    }

}