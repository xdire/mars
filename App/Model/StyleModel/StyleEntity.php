<?php namespace App\Model\StyleModel;

class StyleEntity
{

    /**
     * @var StyleNode[]
     */
    private $styles = [];

    /**
     * @param string      $style
     * @param string|null $vendor
     */
    public function addStyle(string $style, string $vendor = null) {
        $this->styles[] = new StyleNode($style, $vendor);
    }

    /**
     * @return StyleNode[]
     */
    public function getStyles() {
        return $this->styles;
    }

    public function fromArray(array $a) {

        foreach ($a as $style) {

            $vendor = isset($style['vendor']) ? $style['vendor'] : null;

            $this->addStyle($style['name'],$vendor);

        }

    }

    public function toArray() : array {

        $a = [];

        foreach ($this->styles as $style) {

            $a[] = $style->toArray();

        }

        return $a;

    }

    public function toJson() : string {
        return json_encode($this->toArray());
    }

}