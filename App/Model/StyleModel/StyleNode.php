<?php namespace App\Model\StyleModel;

class StyleNode
{
    /** @var int  */
    private $vendor;
    /** @var string  */
    private $name;

    /**
     * StyleNode constructor.
     * @param string      $name
     * @param string|null $vendor
     */
    public function __construct(string $name, string $vendor = null)
    {
        $this->vendor = $vendor;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param int $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function toArray() : array {
        if(!empty($this->vendor))
            return ["name" => $this->getName(), "vendor" => $this->getVendor()];
        else
            return ["name" => $this->getName()];
    }

}