<?php namespace App\Model\StyleModel;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use App\Model\ItemVariationModel\ItemVariationEntity;
use Xdire\Dude\Core\DB\DBO;

class StyleDAO extends DBO
{

    public function getByItemVariation(ItemVariationEntity $e) {

    }

    public function getByStyleName(string $style) {

    }

    public function getByItemVariationId(int $id) {
        
    }

    /**
     * @param int $id
     * @return StyleNode[]
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getStyleByItemId(int $id) : array {

        $uid = WorkingSet::getUserEntity()->getVendorId();

        $query = "SELECT style_code as s ".
            "FROM ItemStyles ".
            "WHERE sysvid = ".$uid." ".
            "AND item_id = ".$id." LIMIT 10;";

        $r = $this->selectBegin($query);

        $a = [];

        while ($row = $this->fetchRow($r)){

            $this->_assembleEntity($a,$row);

        }

        return $a;

    }

    /**
     * @param string $style
     * @return StyleNode[]
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getListByStyleName(string $style) : array {

        $uid = WorkingSet::getUserEntity()->getVendorId();

        $query = "SELECT style_code as s ".
            "FROM ItemStyles ".
            "WHERE sysvid = ".$uid." ".
            "AND style_code LIKE '".StringsHelper::escapeInput($style)."%' LIMIT 256";

        $r = $this->selectBegin($query);

        $a = [];

        while ($row = $this->fetchRow($r)){

            $this->_assembleEntity($a,$row);

        }

        return $a;

    }

    private function _assembleEntity(&$array,&$dataRow) {

        $array[] = new StyleNode($dataRow["s"]);

    }

}