<?php namespace App\Model\ItemAttributes;

/**
 * Anton Repin <dire.one@gmail.com>
 * Date: 2/17/17
 */

use App\Model\AttributeModel\AttributeNode;
use App\Model\ItemVariationModel\ItemVariationEntity;
use Xdire\Dude\Core\DB\DBO;

class ItemAttributesDAO extends DBO
{

    /**
     * @param AttributeNode[]     $attributes
     * @param ItemVariationEntity $variation
     */
    public function linkAttributesToVariation(array $attributes, ItemVariationEntity $variation) {

        foreach ($attributes as $attr) {

            $query = "INSERT INTO ItemAttributes (attr_id, item_vid) VALUES (".$attr->getId().",".$variation->getId().")";
            $this->writeBegin($query);

        }

    }

    /**
     * @param AttributeNode[]     $attributes
     * @param ItemVariationEntity $variation
     */
    public function unlinkAttributesFromVariation(array $attributes, ItemVariationEntity $variation) {

        foreach ($attributes as $attr) {

            $query = "DELETE FROM ItemAttributes WHERE attr_id=".$attr->getId()." AND item_vid=".$variation->getId();
            $this->writeBegin($query);

        }

    }

}