<?php namespace App\Model\Current;
use App\Model\ItemImage\ItemImageIQUploader;
use App\Model\ServiceEntities\UserEntity;
use Drivers\AuthApi\Model\AuthApiSecurity;
use Xdire\Dude\Core\App;

/**
 * Class WorkingSet
 *
 * Working set of system related dependencies
 * which need to be properly set up for DB
 * operations & etc.
 *
 * @package App\Model\Current
 */
class WorkingSet
{
    /** @var UserEntity */
    private static $userEntity;
    /** @var int */
    private static $userId;
    /** @var AuthApiSecurity */
    private static $security;
    /** @var null|ItemImageIQUploader */
    private static $IQuploader = null;

    /**
     * @return UserEntity
     */
    public static function getUserEntity()
    {
        return self::$userEntity;
    }

    /**
     * @param UserEntity $userEntity
     */
    public static function setUserEntity(UserEntity $userEntity)
    {
        self::$userEntity = $userEntity;
    }
    
    /**
     * @return AuthApiSecurity | null
     */
    public static function getSecurity()
    {
        return self::$security;
    }

    /**
     * @param AuthApiSecurity $security
     */
    public static function setSecurity(AuthApiSecurity $security)
    {
        self::$security = $security;
    }

    /** ---------------------------------------------------------------------------------------
     *
     *  ---------------------------------------------------------------------------------------
     */
    public static function initIQUploader()
    {

        $cfg = App::getConfig('iq_uploader');
        if($cfg['type'] == 'local')
            self::$IQuploader = new ItemImageIQUploader(
                $cfg['host'], $cfg['port'], ItemImageIQUploader::LOCAL_CONNECTION, $cfg['key']);
        else
            self::$IQuploader = new ItemImageIQUploader(
                $cfg['host'], $cfg['port'], ItemImageIQUploader::REMOTE_CONNECTION, $cfg['key']);
        
        if(!self::$IQuploader->isConnected()) {
            throw new \RuntimeException("IQUE Service can't connect to server",500);
        }

    }

    /**
     * @return ItemImageIQUploader|null
     */
    public static function getIQUEService()
    {
        if(self::$IQuploader !== null)
            return self::$IQuploader;
        throw new \RuntimeException("IQUE Service not properly initialized",500);
    }

}