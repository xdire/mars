<?php namespace App\Model\Current;

use App\Model\Current\CurrentEntities\ImageCacheNode;
use App\Model\ItemCommon\ItemImage;

/**
 * Class WorkingCache
 * @package App\Model\Current
 */
class WorkingCache
{

    /** @var ImageCacheNode[] */
    private static $imageCache = [];

    /** ------------------------------------------------------------------------------------
     *
     *                              Store Image In Cache
     *
     *  ------------------------------------------------------------------------------------
     *
     * @param ItemImage $i
     * @param string    $cachingWord
     * @param int       $size
     */
    public static function addImageToCache(ItemImage &$i, string $cachingWord, int $size) {
        
        $h = md5($cachingWord);

        $new = $i;
        $new->setCached($h);

        $n = new ImageCacheNode($new,0,$h);
        $n->setInitialMarker($cachingWord);

        self::$imageCache[$h] = $n;
        
    }

    /** ------------------------------------------------------------------------------------
     *
     *                              Retrieve image from Cache
     *
     *  ------------------------------------------------------------------------------------
     *
     * @param ItemImage $i
     * @return ItemImage|null
     */
    public static function &getImageFromCache(ItemImage $i) {

        $h = md5($i->getImageData());
        $r = null;

        if(isset(self::$imageCache[$h])) {

            $in = self::$imageCache[$h];

            if($in->getLinkInitial() == $i->getImageData()) {
                return $in->getImageNode();
            }

        }

        return $r;

    }
    
}