<?php namespace App\Model\Current\CurrentEntities;

use App\Model\ItemCommon\ItemImage;

class ImageCacheNode
{
    // ------------------------
    // Initial incoming
    // ------------------------
    private $linkInitial = "";
    // ------------------------
    // Converted data
    // ------------------------
    /** @var ItemImage | null */
    private $imageNode = null;
    private $size = 0;
    // ------------------------
    // MD5 Marker
    // ------------------------
    private $marker = null;

    /**
     * ImageCacheNode constructor.
     * @param ItemImage $image
     * @param int       $size
     * @param string    $marker
     */
    public function __construct(ItemImage &$image, int $size, string $marker)
    {
        $this->imageNode = &$image;
        $this->size = $size;
        $this->marker = $marker;
    }

    /**
     * @return ItemImage|null
     */
    public function &getImageNode() : ItemImage
    {
        return $this->imageNode;
    }

    /**
     * @return string
     */
    public function getLinkInitial() : string
    {
        return $this->linkInitial;
    }

    /**
     * @return int
     */
    public function getSize() : int
    {
        return $this->size;
    }

    /**
     * @return null|string
     */
    public function getMarker() : string
    {
        return $this->marker;
    }

    /**
     * @param string $linkInitial
     */
    public function setInitialMarker($linkInitial)
    {
        $this->linkInitial = $linkInitial;
    }

}