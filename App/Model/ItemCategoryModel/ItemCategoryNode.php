<?php namespace App\Model\ItemCategoryModel;

class ItemCategoryNode
{
    /** @var int */
    private $id;
    /** @var int */
    private $parentId;
    /** @var string */
    private $name;

    /**
     * @return bool
     */
    public function isEmpty() : bool {
        return isset($this->id) ? true : false;
    }

    /**
     * @return int | null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int | null
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId(int $parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param array $a
     */
    public function fromArray(array $a) {
        $this->id = $a['id'];
        $this->name = $a['name'];
    }

}