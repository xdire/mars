<?php namespace App\Model\ItemCategoryModel;

/**
 * Class ItemCategoryEntity
 * @package App\Model\ItemCategoryModel
 */
class ItemCategoryEntity
{
    /** @var ItemCategoryNode[] */
    private $setOfCategories = [];
    /** @var int */
    private $categoryCount = 0;

    /**
     * @return bool
     */
    public function isEmpty() : bool {
        return ($this->categoryCount == 0) ? true : false;
    }

    /**
     * @return int
     */
    public function getCategoryDepth() : int {

        return $this->categoryCount;

    }

    /**
     * @return ItemCategoryNode | null
     */
    public function getCategoryLast() {

        if($this->categoryCount > 0) {

            return $this->setOfCategories[($this->categoryCount - 1)];

        }
        return null;

    }

    /**
     * @return ItemCategoryNode | null
     */
    public function getCategoryTop() {

        if($this->categoryCount > 0) {

            return $this->setOfCategories[0];

        }
        return null;

    }

    /**
     * @param ItemCategoryNode $n
     */
    public function setCategoryItem(ItemCategoryNode $n) {

        $this->setOfCategories[$this->categoryCount++] = $n;

    }

    /**
     * @return \Generator
     */
    public function &getList() {
        foreach ($this->setOfCategories as &$v) {
            yield $v;
        }
    }

    /**
     * @return array
     */
    public function toArray() : array {

        $a = [];

        foreach ($this->setOfCategories as $cat) {
            $a[] = ["id" => $cat->getId(), "name" => $cat->getName()];
        }

        return $a;

    }

    /**
     * @return string
     */
    public function toJson() : string {
        return json_encode($this->toArray());
    }

    /**
     * @param array $a
     */
    public function fromArray(array $a) {

        $this->setOfCategories = [];

        foreach ($a as $val) {

            $node = new ItemCategoryNode();
            $node->fromArray($val);

            $this->setCategoryItem($node);

        }

    }

}