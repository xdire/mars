<?php namespace App\Model\ItemCategoryModel;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use Xdire\Dude\Core\DB\DBException;
use Xdire\Dude\Core\DB\DBO;

class ItemCategoryDAO extends DBO
{

    /**
     * Save ItemCategory Tree complex sequential structure
     *
     * @param ItemCategoryEntity $e
     * @throws \RuntimeException
     * @return ItemCategoryEntity
     */
    function save(ItemCategoryEntity &$e) : ItemCategoryEntity {

        $currentParent = null;
        $currentRoot = 0;
        $i = 0;

        try {

            $this->transactionStart();
            $rootId = null;

            foreach ($e->getList() as &$cat) {

                if (!$cat->getId()) {

                    // Create branches
                    if ($i !== 0) {

                        $query = "INSERT INTO Cat " .
                            "(sysven, cat_root, cat_pt, cat_name) " .
                            "VALUES (" . WorkingSet::getUserEntity()->getVendorId() . "," . $currentRoot . "," . $currentParent . ",'" .
                            StringsHelper::escapeInput($cat->getName()) . "')";

                        $this->writeBegin($query);

                        if (($id = $this->getLastInsertId()) > 0)
                            $cat->setId($id);

                    }
                    // Create root of the category tree
                    else {

                        $query = "INSERT INTO Cat " .
                            "(sysven, cat_root, cat_pt, cat_name) " .
                            "VALUES (" . WorkingSet::getUserEntity()->getVendorId() . "," . $currentRoot . ",0,'" .
                            StringsHelper::escapeInput($cat->getName()) . "')";

                        $this->writeBegin($query);

                        if (($id = $this->getLastInsertId()) > 0) {
                            $cat->setId($id);
                            $currentRoot = $id;
                            $rootId = true;
                        }

                    }

                } else {

                    if(!isset($rootId)) {
                        $currentRoot = $cat->getId();
                        $rootId = true;
                    }

                }

                $currentParent = $cat->getId();
                $i++;

            }

            $this->transactionCommit();

            return $e;

        } catch (DBException $dbe) {

            $this->transactionCancel();
            throw new \RuntimeException("Categories can't be saved at this moment",500);

        }

    }

    function getById(string $id) {

    }

    function getByName(string $name) {

    }

    function getTreesByName(string $name) {

        $query = "SELECT * FROM Cat as c1 ".
            "LEFT JOIN Cat as c2 ON c1.cat_root = c2.cat_root "
            ."WHERE sysven = ".WorkingSet::getUserEntity()->getVendorId().
            " AND cat_name = '".StringsHelper::escapeInput($name)."'";

        $r = $this->selectBegin($query);

        while ($row = $this->fetchRow($r)) {



        }

    }

    /**
     *
     *
     * @param string $name
     * @return ItemCategoryEntity
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    function getTreeByRootName(string $name) : ItemCategoryEntity {

        $e = new ItemCategoryEntity();

        $query = "SELECT c1.cat_id as mid, c2.cat_id as sid, c1.cat_name as root, c2.cat_pt as parent, c2.cat_name as name FROM Cat as c1 ".
            "LEFT JOIN Cat as c2 ON c1.cat_id = c2.cat_root "
            ."WHERE c1.sysven = ".WorkingSet::getUserEntity()->getVendorId().
            " AND c1.cat_name = '".StringsHelper::escapeInput($name)."'".
            " AND c1.cat_root = 0";

        $r = $this->selectBegin($query);

        $root = null;

        while ($row = $this->fetchRow($r)) {

            if(isset($root)) {

                $cat = new ItemCategoryNode();
                $cat->setId((int)$row['sid']);
                $cat->setParentId((int)$row['parent']);
                $cat->setName((string)$row['name']);
                $e->setCategoryItem($cat);

            } else {

                $root = new ItemCategoryNode();
                $root->setId((int)$row['mid']);
                $root->setParentId(0);
                $root->setName((string)$row['root']);
                $e->setCategoryItem($root);

                if(isset($row['sid'])) {

                    $cat = new ItemCategoryNode();
                    $cat->setId((int)$row['sid']);
                    $cat->setParentId((int)$row['parent']);
                    $cat->setName((string)$row['name']);
                    $e->setCategoryItem($cat);

                }

            }

        }

        return $e;

    }

    function getTreeByRootId(int $id) {



    }

}