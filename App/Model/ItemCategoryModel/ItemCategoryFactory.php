<?php namespace App\Model\ItemCategoryModel;

class ItemCategoryFactory
{

    public static function createFromArray(array $a) : ItemCategoryEntity {

        $c = new ItemCategoryEntity();

        foreach($a as $v) {

            if(isset($v['name'])) {

                $e = new ItemCategoryNode();
                $e->setName($v['name']);

                if(isset($v['id'])){
                    $e->setId($v['id']);
                }

                $c->setCategoryItem($e);

            }

        }

        return $c;

    }


}