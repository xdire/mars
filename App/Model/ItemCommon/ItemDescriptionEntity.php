<?php namespace App\Model\ItemCommon;

class ItemDescriptionEntity
{
    /** @var string  */
    private $short = "";
    /** @var string  */
    private $full = "";
    /** @var array  */
    private $descriptions = [];

    /**
     * @return string
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * @param string $short
     */
    public function setShort($short)
    {
        $this->short = $short;
    }

    /**
     * @return string
     */
    public function getFull()
    {
        return $this->full;
    }

    /**
     * @param string $full
     */
    public function setFull($full)
    {
        $this->full = $full;
    }

    /**
     * @return string
     */
    public function getDescByName(string $name) : string
    {
        if(isset($this->descriptions[$name])) {
            return $this->descriptions[$name];
        }
        return "";
    }

    /**
     * @param string $name
     * @param string $desc
     */
    public function setDescWithName(string $name, string $desc)
    {
        $this->descriptions[$name] = $desc;
    }

    public function fromArray(array $a) {

        foreach ($a as $k => $v) {
            if ($k == 'short')
                $this->setShort($v);
            elseif ($k == 'full')
                $this->setFull($v);
            else
                continue;
        }

    }

    public function toArray() {

        $a = [
            "short" => $this->short,
            "full" => $this->full
        ];

        foreach($this->descriptions as $n => $d){
            $a[$n] = $d;
        }

        return $a;

    }

    public function toJson() {
        return json_encode($this->toArray());
    }

}