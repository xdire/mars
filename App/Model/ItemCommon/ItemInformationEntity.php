<?php namespace App\Model\ItemCommon;

class ItemInformationEntity
{

    /** @var ItemImage */
    private $itemTitleImage;
    /** @var int */
    private $imgAmount = 0;
    /** @var ItemImage[] */
    private $itemImagesSet = [];
    /** @var int */
    private $bulAmount = 0;
    /** @var array */
    private $bullets = [];

    /**
     * @return ItemImage | null
     */
    public function &getItemTitleImage() {

        $r = null;
        if(isset($this->itemTitleImage))
            return $this->itemTitleImage;
        else {
            $r = null;
            return $r;
        }

    }

    /**
     * @param ItemImage $itemTitleImage
     */
    public function setItemTitleImage(ItemImage $itemTitleImage) {
        $this->itemTitleImage = $itemTitleImage;
    }

    /**
     * @return \App\Model\ItemCommon\ItemImage[]
     */
    public function &getItemImagesSet() {
        return $this->itemImagesSet;
    }

    /**
     * @param \App\Model\ItemCommon\ItemImage[] $imageSet
     */
    public function setItemImagesSet(array $imageSet) {
        $this->itemImagesSet = $imageSet;
    }

    /**
     * @param \App\Model\ItemCommon\ItemImage $itemImage
     */
    public function addItemImage(ItemImage $itemImage) {
        $this->itemImagesSet[$this->imgAmount++] = $itemImage;
    }

    /**
     * @param string $bullet
     */
    public function addBullet(string $bullet) {

        $this->bullets[$this->bulAmount++] = $bullet;

    }

    /**
     * @return array
     */
    public function getBullets()
    {
        return $this->bullets;
    }

    /**
     * @return array
     */
    public function toArray() : array {

        $a = [];
        $imgs = null;

        if(isset($this->itemTitleImage) || $this->imgAmount > 0) {
            $a['images'] = [];
            $imgs = &$a['images'];
        }

        if(isset($this->itemTitleImage)) {

            $imgs['titleImage'] = [
                'id'=> $this->itemTitleImage->getId(),'src' => $this->itemTitleImage->getImageData(),'ext' => $this->itemTitleImage->getImageExt(),'desc' => $this->itemTitleImage->getDescription()
            ];

        }

        foreach ($this->itemImagesSet as $img) {

            $imgs['imageSet'][] = [
                'id'=>$img->getId(),'src' => $img->getImageData(),'ext' => $img->getImageExt(),'desc' => $img->getDescription()];

        }

        if($this->bulAmount > 0){

            $a['bullets'] = [];

            foreach($this->bullets as $b) {

                $a['bullets'][] = $b;

            }

        }

        return $a;

    }

    /**
     * @return string
     */
    public function toJson() : string {
        return json_encode($this->toArray(),JSON_UNESCAPED_SLASHES);
    }

    /**
     * @param array $a
     */
    public function fromArray(array $a) {

        // Render images
        if (isset($a['images'])) {

            $imagesBlock = &$a['images'];

            // Put title image into info block object
            if (isset($imagesBlock['titleImage'])) {

                $id = isset($imagesBlock['titleImage']['id']) ? $imagesBlock['titleImage']['id'] : null;
                $name = isset($imagesBlock['titleImage']["name"]) ? $imagesBlock['titleImage']["name"] : "";
                $ext = isset($imagesBlock['titleImage']['ext']) ? $imagesBlock['titleImage']['ext'] : "";

                $this->setItemTitleImage(
                    new ItemImage(
                        $id,
                        $imagesBlock['titleImage']["src"],
                        $name,
                        $imagesBlock['titleImage']["desc"],
                        $ext));

            }

            // Put image set
            if (isset($imagesBlock['imageSet'])) {

                foreach($imagesBlock['imageSet'] as $imageSource) {

                    $id = isset($imageSource['id']) ? $imageSource['id'] : null;
                    $name = isset($imageSource["name"]) ? $imageSource["name"] : "";
                    $ext = isset($imageSource["ext"]) ? $imageSource["ext"] : "";

                    $this->addItemImage(
                        new ItemImage(
                            $id,
                            $imageSource["src"],
                            $name,
                            $imageSource["desc"],
                            $ext));

                }

            }

        }

        // Render bullets
        if (isset($a['bullets'])) {

            foreach($a['bullets'] as $b) {
                $this->addBullet($b);
            }

        }

    }

}