<?php namespace App\Model\ItemCommon;

use App\Model\ItemAbstract\ItemAbstractImage;

class ItemImage extends ItemAbstractImage
{

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $fileName;
    /**
     * @var string
     */
    private $description;
    /**
     * @var bool
     */
    private $inBucket = false;
    /**
     * @var string | null
     */
    private $cached = null;

    /**
     * ItemImage constructor.
     * @param null   $id
     * @param string $imageData
     * @param string $fileName
     * @param string $imageDescription
     * @param string $imageExtension
     */
    function __construct(
        $id = null, 
        string $imageData,
        string $fileName = "",
        string $imageDescription = "",
        string $imageExtension = "")
    {
        $this->id = $id;
        $this->imageData = $imageData;
        $this->fileName = $fileName;
        $this->description = $imageDescription;
        $this->ext = $imageExtension;
    }

    /**
     * @return bool|null|string
     */
    public function isCached() {
        if(isset($this->cached)) {
            return $this->cached;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool {
        return isset($this->id) ? false : true;
    }

    /**
     * @return int | null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTypeOfImage() : int
    {
        $this->detectImageTypeOfData();
        return $this->typeOfImage;
    }

    /**
     * @param int $typeOfImage
     */
    public function setTypeOfImage($typeOfImage)
    {
        $this->typeOfImage = $typeOfImage;
    }

    /**
     * @return string
     */
    public function getImageData() : string
    {
        return $this->imageData;
    }

    /**
     * @param string $imageData
     */
    public function setImageData($imageData)
    {
        $this->imageData = $imageData;
    }

    /**
     * @return string
     */
    public function getImageExt()
    {
        return $this->ext;
    }

    /**
     * @param string $ext
     */
    public function setImageExt($ext)
    {
        $this->ext = $ext;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function __isInBucket() : bool
    {
        return $this->inBucket;
    }

    /**
     * @param void
     */
    public function __setAsInBucket()
    {
        $this->inBucket = true;
    }

    /**
     * @param string $cachedIndex
     */
    public function setCached(string $cachedIndex)
    {
        $this->cached = $cachedIndex;
    }

    public function __generateFileName() {

        $suffix = $this->__generateTimestamp();

        if($this->typeOfImage == 0) {

            $a = explode("/",$this->getImageData());
            $name = array_pop($a);

            $ae = explode(".",$name);

            $ext = array_pop($ae);

            if(($nExt = strstr($ext, '?', true)) !== false) {
                $ext = $nExt;
            }

            /* if($ext != "jpg" || $ext != "jpeg" || $ext != "png" || $ext != "webp" || $ext != "gif") {

            }*/

            $name = implode("",$ae);

            if(strlen($name) > 64) {
                $name = substr($name, 0, 64);
            }

            return $name.$suffix.".".$ext;

        } else {

            return $suffix;

        }

    }

    public function __generateTimestamp() {

        $suffix = microtime();
        list($usec, $sec) = explode(" ", $suffix);
        return $sec.(substr($usec+1,2,5));

    }

}