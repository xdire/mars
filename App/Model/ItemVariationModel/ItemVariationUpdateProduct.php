<?php namespace App\Model\ItemVariationModel;

/**
 * Created by Anton Repin.
 * Date: 7/12/16
 * Time: 12:15 PM
 */

use App\Model\AttributeModel\AttributeNode;
use App\Model\AttributeModel\AttributesEntity;
use App\Model\ColorModel\ColorEntity;
use App\Model\ItemCommon\ItemImage;
use App\Model\SizeModel\SizeEntity;
use App\Model\System\Nodes\TypedNode;

class ItemVariationUpdateProduct
{

    private $attributesToUnlink = [];

    private $attributesToLink = [];

    private $initialEntity = null;

    private $stagingEntity = null;

    private $cachedFieldsToUpdate = [];

    private $entityFieldsToUpdate = [];

    /** @var ColorEntity|null  */
    private $color = null;
    /** @var SizeEntity|null */
    private $size = null;
    /** @var int|null  */
    private $parent = null;
    /** @var string|null  */
    private $code = null;
    /** @var float|null  */
    private $weight = null;
    /** @var float|null  */
    private $height = null;
    /** @var float|null  */
    private $width = null;
    /** @var float|null  */
    private $price = null;

    private $attributes = null;
    /** @var string|null  */
    private $customId = null;

    function __construct(ItemVariationEntity $initial, ItemVariationEntity $staging) {
        $this->initialEntity = $initial;
        $this->stagingEntity = $staging;
    }

    /**
     * @return ItemVariationEntity|null
     */
    public function getInitialEntity() {
        return $this->initialEntity;
    }

    /**
     * @return ItemVariationEntity|null
     */
    public function getStagingEntity() {
        return $this->stagingEntity;
    }

    public function addImageToUpdate(ItemImage $image, int $type) {
        
    }

    public function addAttributeToLink(AttributeNode $node) {
        $this->attributesToLink[] = $node;
    }

    public function addAttributeToUnlink(AttributeNode $node) {
        $this->attributesToUnlink[] = $node;
    }

    /**
     * @return AttributeNode[]
     */
    public function getAttributesToUnlink(): array
    {
        return $this->attributesToUnlink;
    }

    /**
     * @return AttributeNode[]
     */
    public function getAttributesToLink(): array
    {
        return $this->attributesToLink;
    }

    public function changeParent(int $parent) {
        $this->parent = $parent;
    }

    public function isParentChanged() {
        return $this->parent !== null;
    }
    public function changeColor(ColorEntity $e) {
        $this->color = $e;
    }

    public function isColorChanged() {
        return $this->color !== null;
    }

    public function changeSize(SizeEntity $e) {
        $this->size = $e;
    }

    public function isSizeChanged() {
        return $this->size !== null;
    }

    public function changeCode(string $code) {
        $this->code = $code;
    }

    public function isCodeChanged() {
        return $this->code !== null;
    }

    public function changeWeight(float $weight) {
        $this->weight = $weight;
    }

    public function isWeightChanged() {
        return $this->weight !== null;
    }

    public function changeWidth(float $width) {
        $this->width = $width;
    }

    public function isWidthChanged() {
        return $this->width !== null;
    }

    public function changeHeight(float $height) {
        $this->height = $height;
    }

    public function isHeightChanged() {
        return $this->height !== null;
    }

    public function changePrice(float $price) {
        $this->price = $price;
    }

    public function isPriceChanged() {
        return $this->price !== null;
    }

    public function changeCustomId(string $customId) {
        $this->customId = $customId;
    }

    public function isCustomIdChanged() {
        return $this->customId !== null;
    }

    public function changeAttributes(AttributesEntity $e) {
        $this->attributes = $e;
    }

    public function isAttributesChanged() {
        return $this->attributes !== null;
    }

    /**
     * @return ColorEntity|null
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return SizeEntity|null
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return int|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return float|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return float|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return float|null
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return null|string
     */
    public function getCustomId()
    {
        return $this->customId;
    }

    /**
     * @return null|AttributesEntity
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

}