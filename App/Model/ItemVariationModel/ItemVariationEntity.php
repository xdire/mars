<?php namespace App\Model\ItemVariationModel;

use App\Model\AttributeModel\AttributesEntity;
use App\Model\ColorModel\ColorEntity;
use App\Model\ItemCommon\ItemInformationEntity;
use App\Model\SizeModel\SizeEntity;
use App\Model\StyleModel\StyleEntity;

class ItemVariationEntity
{
    /** @var int */
    private $id;

    /** @var string */
    private $customId;

    /** @var int */
    private $parentId;
    /** @var int */
    private $ownerId;

    /** @var string  */
    private $title = "";
    /** @var string  */
    private $brand = "";

    /** @var StyleEntity */
    private $style;
    /** @var SizeEntity */
    private $size;
    /** @var ColorEntity */
    private $color;
    /** @var AttributesEntity */
    private $attributes;

    /** @var string */
    private $code;
    /** @var float */
    private $weight = 0;
    /** @var float */
    private $height = 0;
    /** @var float */
    private $width = 0;
    /** @var float */
    private $price = 0;

    /** @var ItemInformationEntity */
    private $infoObject;

    function __construct()
    {
        //$this->style = new StyleEntity();
        $this->size = new SizeEntity("");
        $this->color = new ColorEntity("");
        $this->attributes = new AttributesEntity();
        $this->infoObject = new ItemInformationEntity();
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool {
        return isset($this->id) ? false : true;
    }

    /**
     * @return int
     */
    public function getId() : int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) {
        $this->id = (int)$id;
    }

    /**
     * @return int
     */
    public function getParentId() : int {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId(int $parentId) {
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getOwnerId() : int {
        return $this->ownerId;
    }

    /**
     * @param int $ownerId
     */
    public function setOwnerId(int $ownerId) {
        $this->ownerId = $ownerId;
    }

    /**
     * @return string
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code) {
        $this->code = $code;
    }

    /**
     * @return float
     */
    public function getWeight() : float {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight) {
        $this->weight = $weight;
    }

    /**
     * @return float
     */
    public function getHeight() : float {
        return $this->height;
    }

    /**
     * @param float $height
     */
    public function setHeight(float $height) {
        $this->height = $height;
    }

    /**
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param float $width
     */
    public function setWidth(float $width)
    {
        $this->width = $width;
    }

    /**
     * @return float
     */
    public function getPrice() : float {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price) {
        $this->price = $price;
    }

    /**
     * @return SizeEntity
     */
    public function &getSize() : SizeEntity {
        return $this->size;
    }

    /**
     * @param SizeEntity $size
     */
    public function setSizeObject(SizeEntity $size) {
        $this->size = $size;
    }

    /**
     * @return ColorEntity
     */
    public function &getColor() : ColorEntity {
        return $this->color;
    }

    /**
     * @param ColorEntity $color
     */
    public function setColorObject(ColorEntity $color) {
        $this->color = $color;
    }

    /**
     * @return AttributesEntity
     */
    public function &getAttributes() : AttributesEntity {
        return $this->attributes;
    }

    /**
     * @param AttributesEntity $attributes
     */
    public function setAttributes(AttributesEntity $attributes) {
        $this->attributes = $attributes;
    }

    /**
     * @return ItemInformationEntity
     */
    public function &getInformationObject() : ItemInformationEntity {
        return $this->infoObject;
    }

    /**
     * @param ItemInformationEntity $infoObject
     */
    public function setInformationObject(ItemInformationEntity $infoObject) {
        $this->infoObject = $infoObject;
    }

    /**
     * @return string
     */
    public function getBrand() : string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return StyleEntity
     */
    public function &getStyle()
    {
        return $this->style;
    }

    /**
     * @param StyleEntity $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @return string
     */
    public function getCustomId()
    {
        return $this->customId;
    }

    /**
     * @param string $customId
     */
    public function setCustomId($customId)
    {
        $this->customId = (string)$customId;
    }

    /**
     * @return array
     */
    public function toArray() : array {

        $a = [
            "id" => $this->id,
            "customId" => $this->customId,
            "parentId" => $this->parentId,
            "sizeId" => $this->size->getId(),
            "size" => $this->size->getName(),
            "colorId" => $this->color->getId(),
            "color" => $this->color->getName(),
            "code" => $this->code,
            "weight" => $this->weight,
            "width" => $this->width,
            "height" => $this->height,
            "price" => $this->price,
            "attributes" => $this->attributes->toArray(),
            "info" => $this->infoObject->toArray()
        ];
        return $a;

    }

    /**
     * @return array
     */
    public function toMinArray() : array {

        $a = [
            "id" => $this->id,
            "customId" => $this->customId,
            "parentId" => $this->parentId,
            "sizeId" => $this->size->getId(),
            "colorId" => $this->color->getId(),
            "code" => $this->code,
            "weight" => $this->weight,
            "width" => $this->width,
            "height" => $this->height,
            "price" => $this->price
        ];
        return $a;

    }

    /**
     * @return string
     */
    public function toJson() : string {

        return json_encode($this->toArray());

    }

}