<?php namespace App\Model\ItemVariationModel;

use App\Model\Current\WorkingSet;
use App\Model\Helpers\StringsHelper;
use App\Model\ItemAttributes\ItemAttributesDAO;
use Xdire\Dude\Core\DB\DBO;

class ItemVariationDAO extends DBO
{

    public function save(ItemVariationEntity $e) : ItemVariationEntity {

    }

    public function update(ItemVariationEntity $e) : ItemVariationEntity {

    }

    /**
     * @param int $id
     * @return ItemVariationEntity
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getById(int $id) : ItemVariationEntity {

        $query = "SELECT * FROM ItemVariations LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE sysvid = ".WorkingSet::getUserEntity()->getVendorId()." AND ItemVariations.var_id = ".$id.";";

        $r = $this->selectBegin($query);

        $var = new ItemVariationEntity();

        if($row = $this->fetchRow($r)){
            
            $var->setId((int)$row['var_id']);
            $var->setCustomId($row['custom_vid']);
            $var->setParentId((int)$row['parent_id']);
            $var->setOwnerId((int)$row['sysvid']);

            $var->getSize()->setId((int)$row['size_id']);
            $var->getSize()->setName($row['size_name']);
            $var->getColor()->setId((int)$row['color_id']);
            $var->getColor()->setName($row['color_name']);

            $var->setCode($row['code']);
            $var->setWeight((float)$row['weight']);
            $var->setHeight((float)$row['height']);
            $var->setWidth((float)$row['width']);
            $var->setPrice((float)$row['price']);

            if($attrs = json_decode($row['attributes'],true))
                $var->getAttributes()->fromArray($attrs);
            if($vdata = json_decode($row['vardata'],true))
                $var->getInformationObject()->fromArray($vdata);

        }

        return $var;

    }

    /**
     * @param array $idSet
     * @return ItemVariationEntity[]
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByIdSet(array $idSet) : array {

        $set = [];

        foreach ($idSet as $id) {
            $set[] = "'".StringsHelper::escapeInput($id)."'";
        }
        
        $query = "SELECT * FROM ItemVariations LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE sysvid = ".WorkingSet::getUserEntity()->getVendorId()." AND ItemVariations.var_id IN (".implode(",",$set).");";

        $r = $this->selectBegin($query);

        $a = [];

        while ($row = $this->fetchRow($r)) {

            $var = new ItemVariationEntity();

            $var->setId((int)$row['var_id']);
            $var->setCustomId($row['custom_vid']);
            $var->setParentId((int)$row['parent_id']);
            $var->setOwnerId((int)$row['sysvid']);

            $var->getSize()->setId((int)$row['size_id']);
            $var->getSize()->setName($row['size_name']);
            $var->getColor()->setId((int)$row['color_id']);
            $var->getColor()->setName($row['color_name']);

            $var->setCode($row['code']);
            $var->setWeight((float)$row['weight']);
            $var->setHeight((float)$row['height']);
            $var->setWidth((float)$row['width']);
            $var->setPrice((float)$row['price']);

            if($attrs = json_decode($row['attributes'],true))
                $var->getAttributes()->fromArray($attrs);
            if($vdata = json_decode($row['vardata'],true))
                $var->getInformationObject()->fromArray($vdata);

            $a[] = $var;

        }

        return $a;

    }

    /**
     * @param array $idSet
     * @return ItemVariationEntity[]
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByIdSetMinimized(array $idSet) : array {

        $set = [];

        foreach ($idSet as $id) {
            $set[] = "'".StringsHelper::escapeInput($id)."'";
        }

        $query = "SELECT * FROM ItemVariations LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE sysvid = ".WorkingSet::getUserEntity()->getVendorId()." AND ItemVariations.var_id IN (".implode(",",$set).");";

        $r = $this->selectBegin($query);

        $a = [];

        while ($row = $this->fetchRow($r)) {

            $var = new ItemVariationEntity();

            $var->setId((int)$row['var_id']);
            $var->setCustomId($row['custom_vid']);
            $var->setParentId((int)$row['parent_id']);
            $var->setOwnerId((int)$row['sysvid']);

            $var->getSize()->setId((int)$row['size_id']);
            $var->getColor()->setId((int)$row['color_id']);

            $var->setCode($row['code']);
            $var->setWeight((float)$row['weight']);
            $var->setHeight((float)$row['height']);
            $var->setWidth((float)$row['width']);
            $var->setPrice((float)$row['price']);

            $a[] = $var;

        }

        return $a;

    }

    /**
     * @param string $code
     * @return ItemVariationEntity[]
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByCode(string $code) : array {
        
        $query = "SELECT * FROM ItemVariations ".
            "LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE sysvid = ".WorkingSet::getUserEntity()->getVendorId()." AND ".
            "ItemVariations.code = '".StringsHelper::escapeInput($code)."';";

        $r = $this->selectBegin($query);

        $a = [];

        while ($row = $this->fetchRow($r)) {

            $var = new ItemVariationEntity();

            $var->setId($row['var_id']);
            $var->setCustomId($row['custom_vid']);
            $var->setParentId($row['parent_id']);
            $var->setOwnerId($row['sysvid']);

            $var->getSize()->setId($row['size_id']);
            $var->getSize()->setName($row['size_name']);
            $var->getColor()->setId($row['color_id']);
            $var->getColor()->setName($row['color_name']);

            $var->setCode($row['code']);
            $var->setWeight((float)$row['weight']);
            $var->setHeight((float)$row['height']);
            $var->setWidth((float)$row['width']);
            $var->setPrice((float)$row['price']);

            if($attrs = json_decode($row['attributes'],true))
                $var->getAttributes()->fromArray($attrs);
            if($vdata = json_decode($row['vardata'],true))
                $var->getInformationObject()->fromArray($vdata);
            
            $a[] = $var;

        }

        return $a;
        
    }

    /**
     * @param string[] $customIdSet
     * @return ItemVariationEntity[]
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getByCustomIdSet(array $customIdSet) : array {

        $set = [];

        foreach ($customIdSet as $id) {
            $set[] = "'".StringsHelper::escapeInput($id)."'";
        }

        $query = "SELECT * FROM ItemVariations ".
            "LEFT JOIN VariationCached ON ItemVariations.var_id = VariationCached.varId ".
            "WHERE sysvid = ".WorkingSet::getUserEntity()->getVendorId()." AND ".
            "ItemVariations.custom_vid IN (".implode(",",$set).");";

        $r = $this->selectBegin($query);
        $a = [];

        while ($row = $this->fetchRow($r)) {

            $var = new ItemVariationEntity();

            $var->setId((int)$row['var_id']);
            $var->setCustomId($row['custom_vid']);
            $var->setParentId((int)$row['parent_id']);
            $var->setOwnerId((int)$row['sysvid']);

            $var->getSize()->setId((int)$row['size_id']);
            $var->getSize()->setName($row['size_name']);
            $var->getColor()->setId((int)$row['color_id']);
            $var->getColor()->setName($row['color_name']);

            $var->setCode($row['code']);
            $var->setWeight((float)$row['weight']);
            $var->setHeight((float)$row['height']);
            $var->setWidth((float)$row['width']);
            $var->setPrice((float)$row['price']);

            if($attrs = json_decode($row['attributes'],true))
                $var->getAttributes()->fromArray($attrs);
            if($vdata = json_decode($row['vardata'],true))
                $var->getInformationObject()->fromArray($vdata);

            $a[] = $var;

        }

        return $a;

    }

    /**
     * @param string[] $customIdSet
     * @return ItemVariationEntity[]
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getMinimizedByCustomId(array $customIdSet) : array {

        $set = [];

        foreach ($customIdSet as $id) {
            $set[] = "'".StringsHelper::escapeInput($id)."'";
        }

        $query = "SELECT * FROM ItemVariations ".
            "WHERE sysvid = ".WorkingSet::getUserEntity()->getVendorId()." AND ".
            "ItemVariations.custom_vid IN (".implode(",",$set).");";

        $r = $this->selectBegin($query);
        $a = [];

        while ($row = $this->fetchRow($r)) {

            $var = new ItemVariationEntity();

            $var->setId((int)$row['var_id']);
            $var->setCustomId($row['custom_vid']);
            $var->setParentId((int)$row['parent_id']);
            $var->setOwnerId((int)$row['sysvid']);

            $var->getSize()->setId((int)$row['size_id']);
            $var->getColor()->setId((int)$row['color_id']);

            $var->setCode($row['code']);
            $var->setWeight((float)$row['weight']);
            $var->setHeight((float)$row['height']);
            $var->setWidth((float)$row['width']);
            $var->setPrice((float)$row['price']);

            $a[] = $var;

        }

        return $a;
        
    }

    public function getByItemId(int $id) : array {

    }

    /**
     * @param int[] $variations
     * @return ItemVariationEntity[]
     * @throws \Xdire\Dude\Core\DB\DBReadException
     */
    public function getVariationsCachedObject(array $variations) {
        
        $query = "SELECT * FROM VariationCached WHERE varId IN (".implode(',',$variations).")";

        $r = $this->selectBegin($query);

        $a = [];

        if($r) {

            while ($row = $this->fetchRow($r)) {
                
                $var = new ItemVariationEntity();
                
                $var->setId((int)$row['varId']);
                
                $var->getSize()->setName($row['size_name']);
                $var->getColor()->setName($row['color_name']);
                $var->setBrand($row['brand_name']);
                $var->setTitle($row['title']);
                if($attrs = json_decode($row['attributes'],true))
                    $var->getAttributes()->fromArray($attrs);
                if($vdata = json_decode($row['vardata'],true))
                    $var->getInformationObject()->fromArray($vdata);

                $a[] = $var;

            }

        }

        return $a;
        
    }

    /**
     * @param ItemVariationEntity[] $variations
     */
    public function setVariationsCachedObject(array &$variations) {

        $query = "";

        foreach ($variations as $var) {

            $query .= "UPDATE VariationCached SET title='".StringsHelper::escapeInput($var->getTitle())
                ."',brand_name='".StringsHelper::escapeInput($var->getBrand())
                ."',color_name='".StringsHelper::escapeInput($var->getColor()->getName())
                ."',size_name='".StringsHelper::escapeInput($var->getSize()->getName())
                ."',attributes='".StringsHelper::escapeJson($var->getAttributes()->toJson())
                ."',vardata='".StringsHelper::escapeJson($var->getInformationObject()->toJson())
                ."' WHERE varId = ".$var->getId().";";

        }

        $this->writeBegin($query);

    }

    /**
     * @param ItemVariationEntity[] $variations
     */
    public function setVariationsCachedInfoObject(array &$variations) {

        $query = "";

        foreach ($variations as $var) {

            $query .= "UPDATE VariationCached SET vardata='".
                StringsHelper::escapeJson($var->getInformationObject()->toJson())
                ."' WHERE varId = ".$var->getId().";";

        }

        $this->writeBegin($query);

    }

    public function setVariationUpdateProduct(ItemVariationUpdateProduct $p) {

        $updates = [];
        $cacheUpdates = [];

        if($p->getInitialEntity()->getId()) {

            if($p->isColorChanged()) {
                $updates[] = "color_id=".$p->getColor()->getId();
                $cacheUpdates[] = "color_name='".StringsHelper::escapeInput($p->getColor()->getName())."'";
            }

            if($p->isSizeChanged()) {
                $updates[] = "size_id=".$p->getSize()->getId();
                $cacheUpdates[] = "size_name='".StringsHelper::escapeInput($p->getSize()->getName())."'";
            }

            if($p->isParentChanged()) {
                $updates[] = "parent_id=".$p->getParent();
            }

            if($p->isCodeChanged()) {
                $updates[] = "code='".$p->getCode()."'";
            }

            if($p->isWeightChanged()) {
                $updates[] = "weight=".$p->getWeight();
            }

            if($p->isHeightChanged()) {
                $updates[] = "height=".$p->getHeight();
            }

            if($p->isWidthChanged()) {
                $updates[] = "width=".$p->getWidth();
            }

            if($p->isPriceChanged()) {
                $updates[] = "price=".$p->getPrice();
            }

            if($p->isCustomIdChanged()) {
                $updates[] = "custom_vid='".$p->getCustomId()."'";
            }

            if($p->isAttributesChanged()) {
                $cacheUpdates["attributes"] = "attributes='".
                    StringsHelper::escapeJson($p->getAttributes()->toJson())."'";
            }

        }


        try {

            $this->transactionStart();

            if (count($updates) > 0) {

                $query = "UPDATE ItemVariations SET "
                    . (implode(',', $updates)) .
                    " WHERE var_id = " . $p->getInitialEntity()->getId();

                $this->writeBegin($query);

            }

            $aDAO = new ItemAttributesDAO();

            if ($u = $p->getAttributesToUnlink()) {
                $aDAO->unlinkAttributesFromVariation($u, $p->getInitialEntity());
            }

            if ($l = $p->getAttributesToLink()) {
                $aDAO->linkAttributesToVariation($l, $p->getInitialEntity());
            }

            if (count($cacheUpdates) > 0) {

                $query = "UPDATE VariationCached SET "
                    . (implode(',', $cacheUpdates)) .
                    " WHERE varid = " . $p->getInitialEntity()->getId();

                $this->writeBegin($query);

            }

            $this->transactionCommit();

        } catch (\Exception $e) {

            $this->transactionCancel();

        }

    }

}