<?php
/**
 * Created by Anton Repin.
 * Date: 7/11/16
 * Time: 5:49 PM
 */

namespace App\Model\ItemVariationModel;

use App\Model\ItemAbstract\ItemAbstractFactory;

class ItemVariationFactory extends ItemAbstractFactory
{

    public function createFromJson(string $json) : ItemVariationEntity {
        
        if($data = json_decode($json,true)) {

            return $this->createFromJsonArray($json);

        }

        throw new \Exception("Json data is corrupted", 400);

    }

    public function createFromJsonArray(array $json) : ItemVariationEntity {

        return $this->assembleNewVariation($json);

    }

    public function getLastMetaFieldsObject() : ItemVariationMetaEntity {
        return self::$lastVariationFieldList;
    }

}