<?php
/**
 * Created by Anton Repin.
 * Date: 7/12/16
 * Time: 5:13 PM
 */

namespace App\Model\ItemVariationModel;


class ItemVariationMetaEntity
{

    /** @var bool  */
    public $title = false;
    /** @var bool  */
    public $color = false;
    /** @var bool  */
    public $price = false;
    /** @var bool  */
    public $size = false;
    /** @var bool  */
    public $code = false;
    /** @var bool  */
    public $weight = false;
    /** @var bool  */
    public $height = false;
    /** @var bool  */
    public $width = false;
    /** @var bool  */
    public $info = false;
    /** @var bool  */
    public $attributes = false;
    /** @var bool  */
    public $customId = false;
    /** @var bool  */
    public $parentId = false;

}