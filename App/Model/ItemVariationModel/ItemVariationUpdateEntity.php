<?php namespace App\Model\ItemVariationModel;

/**
 * Created by Anton Repin.
 * Date: 7/11/16
 * Time: 5:31 PM
 */

use App\Model\AttributeModel\AttributeNode;
use App\Model\AttributeModel\AttributesEntity;
use App\Model\ItemAbstract\ItemAbstractStorageArranger;
use App\Model\ItemCommon\ItemInformationEntity;
use App\Model\System\Nodes\TypedNode;

class ItemVariationUpdateEntity extends ItemAbstractStorageArranger
{

    private $ivd = null;

    function __construct()
    {
        parent::__construct();
        $this->ivd = new ItemVariationDAO();
    }

    public function updateEntityWithIdFromJsonData(string $jsonData) {

        if($data = json_decode($jsonData,true)) {

            $varFactory = new ItemVariationFactory();

            $variation = $varFactory->createFromJsonArray($data);
            $fields = $varFactory->getLastMetaFieldsObject();

            if($variation->isEmpty())
                throw new \Exception("Element Id for update wasn't found", 400);

            $iv = $this->ivd->getById($variation->getId());

            $up = $this->compareAndGetUpdateProduct($iv, $variation, $fields);

            $this->ivd->setVariationUpdateProduct($up);

        }
        
    }
    
    private function compareAndGetUpdateProduct(ItemVariationEntity $initialEntity,
                                                ItemVariationEntity $stagingEntity,
                                                ItemVariationMetaEntity $meta) : ItemVariationUpdateProduct {

        $up = new ItemVariationUpdateProduct($initialEntity, $stagingEntity);

        if($meta->parentId && $initialEntity->getParentId() !== $stagingEntity->getParentId()) {
            $up->changeParent($stagingEntity->getParentId());
        }

        if($meta->size && $initialEntity->getSize()->getName() !== $stagingEntity->getSize()->getName()) {
            $this->_checkVariationSize($stagingEntity);
            $up->changeSize($stagingEntity->getSize());
        }

        if($meta->color && $initialEntity->getColor()->getName() !== $stagingEntity->getColor()->getName()) {
            $this->_checkVariatonColor($stagingEntity);
            $up->changeColor($stagingEntity->getColor());
        }

        if($meta->code && $initialEntity->getCode() !== $stagingEntity->getCode()) {
            $up->changeCode($stagingEntity->getCode());
        }

        if($meta->weight && $initialEntity->getWeight() !== $stagingEntity->getWeight()) {
            $up->changeWeight($stagingEntity->getWeight());
        }

        if($meta->width && $initialEntity->getWidth() !== $stagingEntity->getWidth()) {
            $up->changeWidth($stagingEntity->getWidth());
        }

        if($meta->height && $initialEntity->getHeight() !== $stagingEntity->getHeight()) {
            $up->changeHeight($stagingEntity->getHeight());
        }

        if($meta->price && $initialEntity->getPrice() !== $stagingEntity->getPrice()) {
            $up->changePrice($stagingEntity->getPrice());
        }

        if($meta->customId && $initialEntity->getCustomId() !== $stagingEntity->getCustomId()) {
            $up->changeCustomId($stagingEntity->getCustomId());
        }

        if($meta->attributes) {
            $this->_checkVariationAttributes($stagingEntity);
            $this->_compareAttributes($initialEntity->getAttributes(), $stagingEntity->getAttributes(), $up);
            $up->changeAttributes($stagingEntity->getAttributes());
        }

        // TODO CONTINUE TO CHANGE OBJECTS
        if($meta->info) {

        }

        return $up;
        
    }

    private function _compareInfoBlock(ItemInformationEntity $initial,
                                      ItemInformationEntity $staging,
                                      ItemVariationUpdateProduct &$product) {
        



    }

    private function _compareAttributes(AttributesEntity $initial,
                                        AttributesEntity $staging,
                                        ItemVariationUpdateProduct &$product) {


        $stgAttributes = $this->_cmpAttributesGetReindexAttributes($staging);
        $iniAttributes = $this->_cmpAttributesGetReindexAttributes($initial);

        foreach ($stgAttributes as $attr) {

            if(!isset($iniAttributes[$attr->getType().$attr->getName()]))
                $product->addAttributeToLink($attr);

        }

        foreach ($iniAttributes as $attr) {

            if(!isset($stgAttributes[$attr->getType().$attr->getName()]))
                $product->addAttributeToUnlink($attr);

        }

    }

    /**
     * @param AttributesEntity $e
     * @return AttributeNode[]
     */
    private function _cmpAttributesGetReindexAttributes(AttributesEntity $e) {

        $a = [];

        foreach ($e->getAttributes() as $attribute) {
            $a[$attribute->getType().$attribute->getName()] = $attribute;
        }

        return $a;

    }

}