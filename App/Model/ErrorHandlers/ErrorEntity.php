<?php namespace App\Model\ErrorHandlers;

class ErrorEntity
{

    /** @var int */
    private $code;
    /** @var string */
    private $message;

    /**
     * ErrorEntity constructor.
     * @param int    $code
     * @param string $message
     */
    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }
    
    /**
     * @return int
     */
    public function getCode() : int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }


    public function toArray() : array {
        return [
            'errorCode'=>$this->code,
            'errorMessage'=>$this->message
        ];
    }

    public function toJson() : string {
        return json_encode($this->toArray());
    }


}