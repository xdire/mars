<?php namespace App\Model\ErrorHandlers;

/**
 * Class RuntimeError
 *
 * Class defined as Helper to set static Errors output for Response methods
 *
 * @package App\Model\ErrorHandlers
 */
class RuntimeError
{
    /** @var int */
    private static $code;
    /** @var string */
    private static $message;

    /**
     * General inline error setter
     *
     * @param int    $code
     * @param string $message
     * @return RuntimeError
     */
    public static function setError(int $code, string $message) {

        self::$code = $code;
        self::$message = $message;
        return new self;
    }

    /**
     * @return int
     */
    public static function getCode() : int
    {
        return self::$code;
    }

    /**
     * @param int $code
     */
    public static function setCode($code)
    {
        self::$code = $code;
    }

    /**
     * @return string
     */
    public static function getMessage() : string
    {
        return self::$message;
    }

    /**
     * @param string $message
     */
    public static function setMessage($message)
    {
        self::$message = $message;
    }

    /**
     * @return array
     */
    public static function toArray() : array {

        return [
            'errorCode' => self::$code,
            'errorMessage' => self::$message
        ];

    }

    /**
     * @return string
     */
    public static function toJson() : string {

        return json_encode(self::toArray());

    }

}