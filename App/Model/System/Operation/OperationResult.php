<?php namespace App\Model\System\Operation;

class OperationResult
{
    private $status = false;

    private $options = [];

    private $data = null;

    public function toArray()
    {
        $a = ["status" => $this->status];

        if (is_array($this->data)) {
            $a["data"] = $this->data;
        }

        foreach ($this->options as $k => $v) {
            $a[$k] = $v;
        }

        return $a;
    }

    public function toJson() {
        return json_encode($this->toArray(),JSON_UNESCAPED_SLASHES);
    }

    /**
     * @return boolean
     */
    public function isSuccessful()
    {
        return $this->status;
    }

    /**
     *
     */
    public function setSuccesful()
    {
        $this->status = true;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param $key
     * @param $option
     */
    public function setOption($key, $option)
    {
        $this->options[$key] = $option;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


}