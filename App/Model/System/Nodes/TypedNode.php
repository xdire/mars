<?php
/**
 * Created by Anton Repin.
 * Date: 7/12/16
 * Time: 5:55 PM
 */

namespace App\Model\System\Nodes;

class TypedNode
{
    const INTTYPE = 1;
    const STRTYPE = 2;
    const FLTTYPE = 3;

    /** @var mixed */
    private $key;
    /** @var mixed */
    private $value;
    /** @var int */
    private $type;

    /**
     * TypedNode constructor.
     * @param mixed $key
     * @param mixed $value
     * @param int   $type
     */
    public function __construct($key, $value, int $type)
    {
        $this->key = $key;
        $this->value = $value;
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getType() : int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type)
    {
        $this->type = $type;
    }



}