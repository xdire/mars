<?php namespace App\Model\System\Recache;

use App\Model\ItemCommon\ItemInformationEntity;
use App\Model\ItemImage\ItemImageDAO;
use App\Model\ItemImage\ItemImageLinkedStorage;
use App\Model\ItemModel\ItemDAO;
use App\Model\ItemModel\ItemEntity;
use App\Model\ItemVariationModel\ItemVariationDAO;
use App\Model\ItemVariationModel\ItemVariationEntity;
use App\Model\System\Operation\OperationResult;
use Xdire\Dude\Core\DB\DBO;

class RecacheItemImages extends DBO
{
    /** @var ItemImageLinkedStorage  */
    private $imgLinkData = null;

    /**
     * @param int[] $imageId
     * @return OperationResult
     */
    public function recacheByImageIdRange(array $imageId) : OperationResult {

        $idao = new ItemDAO();
        $imgdao = new ItemImageDAO();
        $vdao = new ItemVariationDAO();

        $this->imgLinkData = $imgdao->getByIdRange($imageId);

        $variations = $vdao->getVariationsCachedObject($this->imgLinkData->getVariationIdList());

        $items = $idao->getItemsCachedObject($this->imgLinkData->getItemIdLIst());

        $this->refreshImageDataOnEntity($variations);
        $this->refreshImageDataOnEntity($items);
        
        $idao->setItemsCachedInfoObject($items);
        $vdao->setVariationsCachedInfoObject($variations);

        $ores = new OperationResult();
        $ores->setSuccesful();
        $ores->setOption("items",count($items));
        $ores->setOption("variations",count($variations));
        return $ores;

    }

    /**
     * @param ItemEntity[]|ItemVariationEntity[] $a
     */
    public function refreshImageDataOnEntity(array &$a) {

        foreach ($a as &$e){
            $this->refreshImageDataInInfoObject($e->getInformationObject());
        }

    }

    /**
     * @param ItemInformationEntity $e
     */
    public function refreshImageDataInInfoObject(ItemInformationEntity &$e) {

        if($e->getItemTitleImage()) {
            if ($image = $this->imgLinkData->getItemImageWithId($e->getItemTitleImage()->getId())) {
                $e->setItemTitleImage($image);
            }
        }

        foreach ($e->getItemImagesSet() as &$img) {

            if($image = $this->imgLinkData->getItemImageWithId($img->getId())){
                $img = $image;
            }

        }
        
    }

}