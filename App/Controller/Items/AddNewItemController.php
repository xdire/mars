<?php namespace App\Controller\Items;

use App\Model\Current\WorkingSet;
use App\Model\ErrorHandlers\ErrorEntity;
use App\Model\ErrorHandlers\RuntimeError;
use App\Model\ItemModel\ItemFactory;
use App\Model\ItemModel\ItemSaveNewEntity;
use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

class AddNewItemController implements RoutingController
{
    public function acceptRoute(Request $request, Response $response)
    {

        set_time_limit(300);

        if(!WorkingSet::getSecurity()->canWrite()) {
            $err = new ErrorEntity(401,"Insufficient rights for current user");
            $response->end(401,$err->toJson());
        }


        WorkingSet::initIQUploader();
        $response->addHeader('Content-Type', 'application/json');

        try {

            $isave = new ItemSaveNewEntity();

            $itemEntity = ItemFactory::createOneFromJson($request->getPostData());
            $isave->save($itemEntity);

            $response->send(200,$itemEntity->toJson());

        } catch (\RuntimeException $e) {

            $response->end($e->getCode(), RuntimeError::setError($e->getCode(), $e->getMessage())->toJson());

        }

    }

}