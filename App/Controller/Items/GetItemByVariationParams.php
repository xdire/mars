<?php namespace App\Controller\Items;

use App\Model\Current\WorkingSet;
use App\Model\ErrorHandlers\ErrorEntity;
use App\Model\ItemModel\ItemDAO;
use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

/**
 * Class GetItemByVariationParams
 *
 * Controller expects next types of variable parameters
 *  - [color]
 *  - [size]
 *  - [style]
 *  - [attribute]
 *
 * @package App\Controller\Items
 */
class GetItemByVariationParams implements RoutingController
{

    /**
     * @param Request  $request
     * @param Response $response
     */
    public function acceptRoute(Request $request, Response $response)
    {

        $response->addHeader('Content-Type', 'application/json');

        if(!WorkingSet::getSecurity()->canRead()) {
            $err = new ErrorEntity(401,"Insufficient read rights for current user");
            $response->end(401,$err->toJson());
        }

        $color = $request->getQueryParameter("color");
        $size = $request->getQueryParameter("size");
        $style = $request->getQueryParameter("style");
        
        $idao = new ItemDAO();

        $entities = $idao->getBySetOfStringParameters($color,$size,$style);

        if(count($entities) > 0) {

            $a = [];
            foreach ($entities as $e){
                $a[] = $e->toArray();
            }

            $response->send(200, json_encode($a));
            return;
        }

        $err = new ErrorEntity(404,"Not found");
        $response->end(404,$err->toJson());

    }

}