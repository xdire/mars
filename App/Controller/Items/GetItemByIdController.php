<?php namespace App\Controller\Items;

use App\Model\Current\WorkingSet;
use App\Model\ErrorHandlers\ErrorEntity;
use App\Model\ItemModel\ItemDAO;
use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

class GetItemByIdController implements RoutingController
{
    public function acceptRoute(Request $request, Response $response)
    {

        $response->addHeader('Content-Type', 'application/json');

        if(!WorkingSet::getSecurity()->canRead()) {
            $err = new ErrorEntity(401,"Insufficient read rights for current user");
            $response->end(401,$err->toJson());
        }

        $idao = new ItemDAO();

        $e = $idao->getById($request->getPathParameter('id'));

        if(!$e->isEmpty()) {
            $response->send(200, $e->toJson());
            return;
        }

        $err = new ErrorEntity(404,"Not found");
        $response->end(404,$err->toJson());

    }

}