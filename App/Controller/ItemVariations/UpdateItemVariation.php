<?php
/**
 * Created by Anton Repin.
 * Date: 7/11/16
 * Time: 4:30 PM
 */

namespace App\Controller\ItemVariations;

use App\Model\Current\WorkingSet;
use App\Model\ErrorHandlers\ErrorEntity;
use App\Model\ItemVariationModel\ItemVariationUpdateEntity;
use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

class UpdateItemVariation implements RoutingController
{

    public function acceptRoute(Request $request, Response $response)
    {

        $response->addHeader('Content-Type', 'application/json');

        if(!WorkingSet::getSecurity()->canRead()) {
            $err = new ErrorEntity(401,"Insufficient read rights for current user");
            $response->end(401,$err->toJson());
        }

        $updateModel = new ItemVariationUpdateEntity();
        $updateModel->updateEntityWithIdFromJsonData($request->getPostData());
        
        $response->send(200,'{"sex":true,"id":'.$request->getPathParameter("id").'}');

    }

}