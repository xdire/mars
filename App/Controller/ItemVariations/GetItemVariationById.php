<?php
/**
 * Created by Anton Repin.
 * Date: 6/23/16
 * Time: 10:50 PM
 */

namespace App\Controller\ItemVariations;

use App\Model\Current\WorkingSet;
use App\Model\ErrorHandlers\ErrorEntity;
use App\Model\ItemVariationModel\ItemVariationDAO;
use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

    
class GetItemVariationById implements RoutingController
{
    public function acceptRoute(Request $request, Response $response) {

        $response->addHeader('Content-Type', 'application/json');

        if(!WorkingSet::getSecurity()->canRead()) {
            $err = new ErrorEntity(401,"Insufficient read rights for current user");
            $response->end(401,$err->toJson());
        }

        $typeOfRequest = $request->getHeader("entity-type");

        $idao = new ItemVariationDAO();

        $prepared = explode(',',$request->getPathParameter('id'));

        // If it's multirequest
        if(count($prepared) > 1) {

            if($typeOfRequest == "short")
                $a = $idao->getByIdSetMinimized($prepared);
            else
                $a = $idao->getByIdSet($prepared);

            if(count($a) > 0) {

                foreach ($a as &$v) {
                    $v = $v->toArray();
                }

                $response->send(200, json_encode($a,JSON_UNESCAPED_SLASHES));
                return;

            }

        }
        // If it's single request
        else {
            $e = $idao->getById($prepared[0]);
            if(!$e->isEmpty()) {
                $response->send(200, $e->toJson());
                return;
            }
        }

        $err = new ErrorEntity(404,"Not found");
        $response->end(404,$err->toJson());
        
    }
}