<?php namespace App\Controller\ItemVariations;

use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

class AddNewItemVariationController implements RoutingController
{
    public function acceptRoute(Request $request, Response $response)
    {

        $response->send(200,"Add variation controller in charge");

    }

}