<?php
/**
 * Created by Anton Repin.
 * Date: 6/24/16
 * Time: 11:57 AM
 */

namespace App\Controller\Styles;

use App\Model\StyleModel\StyleDAO;
use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

class GetStyleByItemId implements RoutingController
{
    
    public function acceptRoute(Request $request, Response $response)
    {

        $dao = new StyleDAO();

        $nodes = $dao->getStyleByItemId((int)$request->getPathParameter("id"));

        if(count($nodes) > 0){

            $a = [];
            foreach ($nodes as $node) {
                $a[] = $node->getName();
            }
            $response->send(200,json_encode($a));

        } else {

            $response->end(404);

        }

    }
    
}