<?php namespace App\Controller\Styles;

use App\Model\StyleModel\StyleDAO;
use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

class GetStyleList implements RoutingController
{
    
    public function acceptRoute(Request $request, Response $response)
    {
        
        $dao = new StyleDAO();
        
        $nodes = $dao->getListByStyleName($request->getPathParameter("code"));
        
        if(count($nodes) > 0){

            $a = [];
            foreach ($nodes as $node) {
                $a[] = $node->getName();
            }
            $response->send(200,json_encode($a));

        } else {

            $response->end(404);

        }
        
    }

}