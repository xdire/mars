<?php namespace App\Controller\System\Recache;

use App\Model\System\Operation\OperationResult;
use App\Model\System\Recache\RecacheItemImages;
use Xdire\Dude\Core\Face\RoutingController;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

class ImageRecache implements RoutingController
{
    public function acceptRoute(Request $request, Response $response) {
        
        $operation = $this->_recacheImage($request->getQueryParameter("range"));

        if($operation->isSuccessful()) {
            $response->send(200,$operation->toJson());
        } else {
            $response->end(500,"Operation cannot be completed");
        }

    }
    
    private function _recacheImage(string $id) {
        
        $data = explode(",",$id);
        $rii = new RecacheItemImages();

        if(count($data) > 0) {
            $result = $rii->recacheByImageIdRange($data);
            return $result;
        }

        return new OperationResult();

    }

}