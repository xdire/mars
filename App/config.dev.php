<?php return [
    /* -----------------------------------------------------------------
    |
    |                       DEV ENVIRONMENT
    |
    /* ----------------------------------------------------------------*/

    /* ------------------------------------------------------------------
     *
     *  Database parameters for connection
     *
     * -----------------------------------------------------------------*/

    # [Mysql Default Connection]
    'mysql_connection' => array(
        'type'=>'mysql',
        'host'=>'localhost',
        'sock'=>'',
        'port'=>'',
        'user' => 'root',
        'password' => '',
        'instance' => 'ItemAPI'
    ),

    # [Mysql Custom Connection] for [ $db->useConnection('mysql_userconnection') ]
    'mysql_customconn' => array(
        'type'=>'mysql',
        'host'=>'localhost',
        'sock'=>'',
        'port'=>'',
        'user' => '',
        'password' => '',
        'instance' => ''
    ),


    /* ------------------------------------------------------------------
     *
     *  Parameters Specific for Marketplaces
     *
     * -----------------------------------------------------------------*/

    'image_storage' => [
        'type' => 'amazons3',
        'bucket' => 'afimg',
        'host' => '',
        'keyId'  => 'AKIAJS45UYLGXKS7OKQQ',
        'keySecret' => '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'
    ],
    'iq_uploader' => [
        'type' => 'local',
        'host' => '/var/run/iq.sock',
        'port' => '',
        'key' => 'das4329!#(dsa9*jd--_423ik'
    ],
    'defaultItemImage' => [
        "src"=>"https://s3.amazonaws.com/afimg/v1/uploading",
        "ext"=>"png"
    ]
];