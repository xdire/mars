<?php namespace App\Middleware;

use Xdire\Dude\Core\Face\OptionsMiddleware;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

class OptMiddleware implements OptionsMiddleware
{
    public function start(Request $request, Response $response)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: AuthUser,AuthKey,AuthAgent,UserAgentVer");
        header("Access-Control-Allow-Methods: *");

        http_response_code(200);
    }

}