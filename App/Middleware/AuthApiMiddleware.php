<?php namespace App\Middleware;

use App\Model\Current\WorkingSet;
use App\Model\ErrorHandlers\ErrorEntity;
use App\Model\ServiceEntities\UserEntity;
use Drivers\AuthApi\AuthApiConnector;
use Xdire\Dude\Core\Face\Middleware;
use Xdire\Dude\Core\Server\Request;
use Xdire\Dude\Core\Server\Response;

/**
 * Class AuthApiMiddleware
 * @package App\Middleware
 */
class AuthApiMiddleware implements Middleware
{

    /**
     * Authorize Request Process
     *
     * @param Request  $request
     * @param Response $response
     * @param callable $next
     * @throws \Exception
     */
    public function start(Request $request, Response $response, callable $next)
    {
        if(($stagingUser = $request->getAuthUser()) && ($stagingKey = $request->getAuthKey())) {

            $conn = new AuthApiConnector("http://auth.afapi.net");
            $uobj = $conn->selectMethod()->authorizeVendorKey($stagingUser, $stagingKey);

            // If Auth Api answered positive
            if($uobj->getStatus() == 200){

                $response->addHeader("Access-Control-Allow-Origin","*");
                $response->addHeader("Access-Control-Allow-Headers","AuthUser,AuthKey,AuthAgent,UserAgentVer");
                $response->addHeader("Access-Control-Allow-Methods","*");
                
                $user = new UserEntity();
                $user->setVendorId($uobj->getVendorId());
                $user->setId($uobj->getUserId());
                $request->__setUser($user);
                
                WorkingSet::setUserEntity($user);
                WorkingSet::setSecurity($uobj->getSecurity());

                // Proceed to Controller
                $next($request,$response);
                return;

            }

        }

        // Produce error if key can't be authorized
        $err = new ErrorEntity(401,"Key can't be authorized");
        $response->end(401,$err->toJson());

    }

}