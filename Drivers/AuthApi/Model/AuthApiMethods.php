<?php namespace Drivers\AuthApi\Model;

class AuthApiMethods
{

    private $conn;

    /**
     * AuthApiMethods constructor.
     * @param AuthApiConnection $conn
     */
    public function __construct(AuthApiConnection &$conn){
        $this->conn = $conn;
    }

    /**
     * @param string $keyUser
     * @param string $key
     * @return AuthApiResonse
     * @throws \Drivers\AuthApi\Errors\AuthApiException
     */
    public function authorizeVendorKey(string  $keyUser, string $key) {

        $newData = new AuthApiConnParams();

        $newData->setRequestType(AuthApiConnParams::GET);
        $newData->setHeader("AuthUser: ".$keyUser);
        $newData->setHeader("AuthKey: ".$key);
        $newData->setPath("/api/v1/vendor/auth");

        return $this->conn->execute($newData);

    }

}