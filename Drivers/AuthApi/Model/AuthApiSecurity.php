<?php namespace Drivers\AuthApi\Model;

class AuthApiSecurity
{

    /** @var bool */
    private $read = false;
    /** @var bool */
    private $write = false;
    /** @var bool */
    private $execute = false;
    /** @var int | null */
    private $groupId = null;
    /** @var string | null */
    private $groupName = null;
    /** @var int | null */
    private $groupId2 = null;
    /** @var string | null */
    private $groupName2 = null;
    /** @var int | null */
    private $groupId3 = null;
    /** @var string | null */
    private $groupName3 = null;

    /** @var int */
    private $minGroup = 255;
    /** @var array */
    private $groupSet = [];

    /**
     * AuthApiSecurity constructor.
     * @param array $a
     */
    function __construct(array $a)
    {
        foreach ($a as $k => $v) {

            switch ($k[0]) {

                case "r":
                    $this->read = $v;
                    break;

                case "w":
                    $this->write = $v;
                    break;

                case "x":
                    $this->execute = $v;
                    break;

                case "g":
                    if($k == "gid")
                        $this->groupId = $v;
                    elseif($k == "gid1")
                        $this->groupId2 = $v;
                    else
                        $this->groupId3 = $v;
                    if($v < $this->minGroup)
                        $this->minGroup = $v;
                    break;

                case "l":
                    if($k == "lid")
                        $this->groupName = $v;
                    elseif($k == "lid1")
                        $this->groupName2 = $v;
                    else
                        $this->groupName3 = $v;

                    $this->groupSet[$v] = 1;
                    break;

                default: break;

            }

        }

    }

    /**
     * @param string $group
     * @return bool
     */
    public function isInGroup(string $group)
    {
        return isset($this->groupSet[$group]);

    }

    /**
     * @return int
     */
    public function getTopGroupId()
    {
        return $this->minGroup;
    }

    /**
     * @return boolean
     */
    public function canRead()
    {
        return $this->read;
    }

    /**
     * @return boolean
     */
    public function canWrite()
    {
        return $this->write;
    }

    /**
     * @return boolean
     */
    public function canExecute()
    {
        return $this->execute;
    }

    /**
     * @return int|null
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @return null|string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @return int|null
     */
    public function getGroupId2()
    {
        return $this->groupId2;
    }

    /**
     * @return null|string
     */
    public function getGroupName2()
    {
        return $this->groupName2;
    }

    /**
     * @return int|null
     */
    public function getGroupId3()
    {
        return $this->groupId3;
    }

    /**
     * @return null|string
     */
    public function getGroupName3()
    {
        return $this->groupName3;
    }

}