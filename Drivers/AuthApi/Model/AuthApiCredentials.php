<?php namespace Drivers\AuthApi\Model;

class AuthApiCredentials
{

    private $clientId = "";

    private $key = "";

    private $secret = "";

    /**
     * AuthApiCredentials constructor.
     * @param string $clientId
     * @param string $key
     * @param string $secret
     */
    public function __construct($clientId, $key, $secret)
    {
        $this->clientId = $clientId;
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

}