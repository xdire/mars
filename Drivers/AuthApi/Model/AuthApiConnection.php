<?php namespace Drivers\AuthApi\Model;

use Drivers\AuthApi\Errors\AuthApiException;

class AuthApiConnection
{

    private $host;

    function __construct(string $host)
    {
        $this->host = $host;
    }

    public function execute(AuthApiConnParams $p) {

        $result = new AuthApiResonse();

        $c = curl_init();
        $url = $this->host.$p->getPath();
        /* ------------------------------------------------
         *              Create query parameters
         * -----------------------------------------------*/
        $qn = 0;
        $qp = "";
        foreach ($p->getQueryParams() as $d => $v) {
            if($qn > 0)
                $qp .= "&";
            $qp .= $d."=".$v;
            $qn++;
        }
        if($qn > 0)
            $url .= "?".urlencode($qp);
        /* ------------------------------------------------
         *                      Setup CURL
         * -----------------------------------------------*/
        curl_setopt($c, CURLOPT_URL, $url);
        if($p->getRequestType() > 0) {
            curl_setopt($c, CURLOPT_CUSTOMREQUEST, $p->getRequestMethod());
            curl_setopt($c, CURLOPT_POSTFIELDS, $p->getPostData());
        }
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($c, CURLOPT_HEADER,true);
        curl_setopt($c, CURLOPT_HTTPHEADER, $p->getHeaders());
        /* ------------------------------------------------
         *                      Execute
         * -----------------------------------------------*/
        $response = curl_exec($c);
        
        if($response === false){
            throw new AuthApiException("Connection failed", 500);
        }
        /* ------------------------------------------------
         *                  Setup Response
         * -----------------------------------------------*/
        $status = (int) curl_getinfo($c, CURLINFO_HTTP_CODE);

        $result->setStatus($status);
        $result->setData($response);
        curl_close($c);

        return $result;

    }

}