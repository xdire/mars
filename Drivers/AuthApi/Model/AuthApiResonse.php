<?php namespace Drivers\AuthApi\Model;

class AuthApiResonse
{
    /** @var int */
    private $status;

    /** @var array */
    private $data;
    
    /** @var int */
    private $userId;

    /** @var int */
    private $vendorId;
    
    /** @var AuthApiSecurity */
    private $security;

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData(string $data)
    {
        if($d = json_decode($data,true)) {
            if(isset($d['user'])) {

                if(isset($d['vendor']))
                    $this->vendorId = $d['vendor'];

                $this->userId = $d['user'];
                $this->security = new AuthApiSecurity($d['security']);
                return;
                
            }
        }
    }

    /**
     * @return AuthApiSecurity
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

}