<?php namespace Drivers\AuthApi\Errors;

class AuthApiException extends \Exception
{

    public function __construct($message,$code)
    {
        parent::__construct($message,$code);
    }

}