<?php namespace Drivers\AuthApi;

use Drivers\AuthApi\Model\AuthApiConnection;
use Drivers\AuthApi\Model\AuthApiMethods;

class AuthApiConnector
{

    private $conn;
    
    function __construct(string $host)
    {
        $this->conn = new AuthApiConnection($host);
    }

    function selectMethod() {
        return new AuthApiMethods($this->conn);
    }


}